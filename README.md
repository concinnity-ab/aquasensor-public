# Blue-Green Aquasensor

## Description
This is a simple system for gathering and visualizing sensor data.

The font end runs on a Raspberry Pi and collects data once a minute, buffers it and sends it to the back end when there is a connection.

The back end is packaged for Docker as three containers: an Nginx web server, a Graphite data receiver and database and a Grafana server that makes beautiful web dashboards.

## Licenses
Blue-Green Aquasensor code for sensor nodes and backend configuration is available under AGPL3. Other software (Docker etc.) is available under their respective licenses.

### Front end: the Sensornode daemon
The front end can be configured to sample a variety of hardware connected through OneWire or I2C buses, serial ports and GPIO pins.
It can also send alarms via an SMS modem and/or a PagerDuty REST API, and show status on LEDs connected to GPIO pins.

It is recommended to be used with a local Graphite proxy like Grafsy so that no data is lost in case connection to the Graphite server(s) is temporarily down.

The normal sampling is done at the top of every whole minute. (The exception is the EC measurements which are performed 30s earlier. This is so that if EC and pH probes are mounted in close proximity, the voltage applied to the EC probe will not affect the pH probe.)

Only one OneWire bus is supported, but you can connect many sensors to it. It is configured in /boot/config.txt. It uses GPIO pin 4, which must have a 4,7 kΩ pull-up resistor to 3.3V.

A real-time clock is recommended. It will keep the time correctly if you (re)start without an internet connection. It too is configured in /boot/config.txt and connects through I2C. (The DSL1307 is not recommended as it is very inaccurate - it has no temperature compensation.)

### Back end: the docker containers
 The back end is currently written to run on Debian; with minor tweaks it could also run on a Raspberry Pi.
 See he instructions in the documentation/backend/ folder. 

## Some of the sensor hardware types supported:

### Dallas DS18B20
Temperature. OneWire connection trough GPIO pin 4.

### Dallas DS2423P
Two pulse counters, useful for electricity and water meters.

### Concinnity Klimatlåda
Temperature (C), humidity (%), CO2-level (ppm), Air pressure (mbar) and light level (lux). Also 4 status LEDs.

USB connection. (Arduino based.)

### ADS1015 A/D converter
Four channel volt meter. One channel may be connected to a reference voltage.

I2C connection.

### MHZ14A CO2 sensor
Measures how many ppm CO2 is in the air. Cannot measure below 400 ppm.

Connected via serial port (GPIO 14 and 15)

### Sensirion SCD30 CO2+T+RH sensor
Measures how many ppm CO2 is in the air, as well as temperature and humidity. Can measure below 400 ppm.

I2C connection.

## Front end Installation, Raspberry Pi OS, version "Buster"

1. Clone this repo into /home/pi/aquasensor: `git clone git@gitlab.com:concinnity-ab/aquasensor-public.git aquasensor`
2. `cd aquasensor/setup/`
3. Read setup.sh and make necessary changes to it and /root/config.txt.
4. Use the std Raspbian config tool to turn on I2C, serial port and ssh as required.
5. Reboot.
6. Configure and install Grafsy proxy (optional):
    1. Configure the Graphite server(s) to use and the channel names: `nano ../config/grafsy.toml`
    2. `sudo ./grafsy_setup.sh`
7. Now install sensornode and collectd services: `sudo ./setup.sh`
8. Edit the config to select your hardware: `nano ../bin/sensornode.conf`
8. Start the ball rolling: `sudo systemctl start grafsy` then `sudo systemctl start sensornode`
