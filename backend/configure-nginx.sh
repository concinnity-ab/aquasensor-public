#!/usr/bin/env bash

set -e

function log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') $1"
}

NIC_ID=$1
DEFAULT_ID=en0

if [[ -z "${NIC_ID}" ]]; then
    read -p "Please enter the network interface identifier [${DEFAULT_ID}]: " ID
    export NIC_ID=${ID:-${DEFAULT_ID}}
fi

log "Getting IP address of this machine"
export IP_ADDRESS=$(ip addr show ${NIC_ID}  | \
    awk '$1 == "inet" { print $2 }' | cut -d/ -f1)
log "The IP address is: ${IP_ADDRESS}"

log "Creating nginx config file"
envsubst '${IP_ADDRESS}' \
    <data/nginx/app.conf.template \
    >data/nginx/app.conf
