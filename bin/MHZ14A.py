import serial
import time

# From https://hackaday.io/project/28618/logs
#   The following sequence of bytes turns ABC off:
#     0xFF 0x01 0x79 0x00 0x00 0x00 0x00 0x00 86
#   and this command turns it on:
#     0xFF 0x01 0x79 0xA0 0x00 0x00 0x00 0x00 E6
# This only leaves the question of how persistent a manual calibration is.
# Does is survive power cycling?
                       

class MHZ14A():
    packet        = [0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79]
    manual_zero   = [0xff, 0x87, 0x87, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf2] #sensor # 87??
    auto_zero_off = [0xff, 0x01, 0x79, 0x00, 0x00, 0x00, 0x00, 0x00, 0x86]
    auto_zero_on  = [0xff, 0x01, 0x79, 0xa0, 0x00, 0x00, 0x00, 0x00, 0xe6]
    auto_calibration_was_set = False
    INIT_RETRIES = 6
    
    def __init__(self, port="/dev/ttyS0", auto_calibrate=False):
        self.serial = serial.Serial(port, 9600, timeout = 1)
        #TODO read and discard any garbage in typeahead buffer?
        
        #repeat this until it succeeds or we time out
        for tries in range(self.INIT_RETRIES):
            time.sleep(2)
            if auto_calibrate == False:
                self.serial.write(bytearray(self.auto_zero_off)) #In a non-400ppm baseline env
            else:
                self.serial.write(bytearray(self.auto_zero_on))
            res = self.serial.read(size = 9)
            res = bytearray(res)
            print("Auto Baseline Calibration set return:", res)
            if len(res) == 9:
                #TODO also compare w std value
                self.auto_calibration_was_set = True
                return
            
            print("Auto Baseline Calibration setting abandoned after {} tries:".format(self.INIT_RETRIES), res)
        

    def get(self):
        self.serial.write(bytearray(self.packet))
        res = self.serial.read(size = 9)
        res = bytearray(res)
        checksum = 0xff & (~(res[1]+res[2]+res[3]+res[4]+res[5]+res[6]+res[7])+1)
        if len(res) != 9:
            raise Exception("Response was not 9 bytes long")
        print(res)
        if res[8] == checksum:
            result = {"ppm":(res[2]<<8)|res[3], "temp":res[4]}
            if not self.auto_calibration_was_set:
                result["warn"] = "Auto Calibration was not set"
            return result
        else:
            raise Exception("Response checksum bad, {} <> {}".format(hex(checksum), res[8]) )

    def close(self):
        self.serial.close


def main():
    co2 = MHZ14A("/dev/ttyAMA0")
    try:
        print(co2.get())
    except:
        print("err")
    co2.close()

if __name__ == '__main__':
    main()
