#!/bin/python3
#
# Sends SMS via 3G modem
# (using Gammu library, which has to be installed).
#
import gammu

class SMS():
    sm = None #Gammu state machine
    
    def __init__(self, ser:str = "/dev/ttyAMA0"): #assumes no other USB serial ports!
        self.sm = gammu.StateMachine()
        self.sm.ReadConfig()
        self.sm.Init()
        netinfo = self.sm.GetNetworkInfo()
        print(netinfo)
        

    def send(self, number:str, msg:str):
        message = {
            'Text': msg,
            'SMSC': {'Location': 1},
            'Number': number
        }

        self.sm.SendSMS(message)

        
    def signal_strength(self):
        netinfo = self.sm.GetSignalQuality()
        return netinfo['SignalPercent']
        
    def close(self):
        pass


