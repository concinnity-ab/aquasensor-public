import board
import busio
from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import ads1015 as pimoroni_ads1015

ads1015_sensors = []

class ads1015_sensor(generic_sensor):
    '''Class for a Pimoroni breakout of TI ADS1015 A/D converter; I2C-connected.
    ±24V capable, 12 bit resolution, 3 channels (4th is ref)'''

    @classmethod
    def confignames(cls) -> List:
        return ["ads1015"]

    def __init__(self, config:List):# "ads1015" i2caddr channel0 channel1 channel2
        super().__init__(config)
        #Pimoroni breakout default addr is 0x48; cutting the trace makes it 0x49
        i2caddr = int(config[1], 16) #Always given in hex.
        if i2caddr > 127 or i2caddr < 0:
            raise ValueError('Invalid I2C address')
        if i2caddr not in [0x48, 0x49]:
            raise ValueError('Wrong I2C address - must be 0x48 or 0x49')
        self.channel0 = config[2]
        self.channel1 = config[3]
        self.channel2 = config[4]

        ads1015 = pimoroni_ads1015.ADS1015(i2c_addr=i2caddr)
        ads1015.set_mode('single')
        ads1015.set_programmable_gain(2.048)
        ads1015.set_sample_rate(1600)
        reference = ads1015.get_reference_voltage()
        print("AD1015 reference voltage: {:6.3f}V".format(reference))
        self.sensor = ads1015

        global ads1015_sensors
        ads1015_sensors.append(self)
        super().register()
        

    def read(self) -> Dict:
        # read all three channels
        try:
            result = {}
            a0 = self.sensor.get_compensated_voltage(channel='in0/ref')
            a1 = self.sensor.get_compensated_voltage(channel='in1/ref')
            a2 = self.sensor.get_compensated_voltage(channel='in2/ref')
            result[self.channel0] = a0
            result[self.channel1] = a1
            result[self.channel2] = a2
            #Kludge for concinnity_ec measurement:
            result['_A0'] = a0
            result['_A1'] = a1
            result['_A2'] = a2
            
            #print("ADS1015 V0 {}  V1 {}  V2 {}".format(result[self.channel0], result[self.channel1], a2))
            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("ADS1015 read failed: " + str(e))

    

    

