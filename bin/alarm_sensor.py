from typing import List
from typing import Dict
from generic_sensor import generic_sensor

alarm_sensors = []

class alarm_sensor(generic_sensor):
    '''A sensor that can send alarms
    It's a sensor so that an SMS modem can report a signal level etc.'''
    @classmethod
    def confignames(cls)->List:
        return ["alarm_sensor"]

    sensor = None
    send_errors = 0
    send_count = 0

    def __init__(self, config:List):
        global alarm_sensors
        super().__init__(config)
        alarm_sensors.append(self)
        super().register()

    def read(self) -> Dict:
        return {} #unless overridden

        
    def trig(self, hostname: str, message: str):
        self.send_count += 1

        
    def clear(self, hostname: str, message: str):
        pass
