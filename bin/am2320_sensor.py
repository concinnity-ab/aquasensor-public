import board
import busio
from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import adafruit_am2320

class am2320_sensor(generic_sensor):
    '''Class for an AM2320 T+RH sensor; I2C-connected'''

    @classmethod
    def confignames(cls) -> List:
        return ["am2320"]

    def __init__(self, config:List):# "am2320" i2caddr tempchannel humchannel
        super().__init__(config)
        i2caddr = int(config[1], 16) #Always given in hex.
        if i2caddr > 127 or i2caddr < 0:
            raise ValueError('Invalid I2C address')
        #address always 0x5C
        if i2caddr != 0x5C:
            raise ValueError('Wrong I2C address - must be 0x5C')
        self.tchannel  = config[2]
        self.hchannel  = config[3]
        self.sensor = adafruit_am2320.AM2320(busio.I2C(board.SCL, board.SDA)) #address is fixed
        super().register()

        
    def read(self) -> Dict:
        # read temp+humidity
        #sometimes goes splat when recently opened
        try:
            values[self.tchannel] = self.sensor.temperature
            values[self.hchannel] = self.sensor.relative_humidity
            #print("AM2320 T {} H {}".format(self.sensor.temperature, self.sensor.relative_humidity))
        except Exception as e:
            self.read_errors += 1
            raise Exception("AM2320 read failed: " + str(e))

    

    

