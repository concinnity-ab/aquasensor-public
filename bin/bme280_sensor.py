import board
import busio
from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import adafruit_bme280

class bme280_sensor(generic_sensor):
    '''Class for a Bosch BME280 T+RH+P sensor; I2C-connected.
    Measures humidity with ±3% accuracy, barometric pressure with ±1 hPa absolute accuraccy, and temperature with ±1.0°C accuracy'''

    @classmethod
    def confignames(cls) -> List:
        return ["bme280"]

    def __init__(self, config:List):# "bme280" i2caddr tempchannel humchannel pressurechannel
        super().__init__(config)
        #Adafruit default address=0x77. Grounding SDO makes it 0x76
        #Pimoroni breakout default is 0x76; cutting the trace makes it 0x77
        i2caddr = int(config[1], 16) #Always given in hex.
        if i2caddr > 127 or i2caddr < 0:
            raise ValueError('Invalid I2C address')
        if i2caddr != 0x76 and i2caddr != 0x77:
            raise ValueError('Wrong I2C address - must be 0x76 or 0x77')
        self.tchannel  = config[2]
        self.hchannel  = config[3]
        self.pchannel  = config[4]
        self.sensor = adafruit_bme280.Adafruit_BME280_I2C(busio.I2C(board.SCL, board.SDA), address=i2caddr)
        super().register()

    def read(self) -> Dict:
        # read temp+humidity+pressure
        try:
            result = {}
            result[self.tchannel] = self.sensor.temperature
            result[self.hchannel] = self.sensor.humidity
            result[self.pchannel] = self.sensor.pressure
            #print("BME280 T {}  H {}  P {}".format(self.sensor.temperature, self.sensor.humidity, self.sensor.pressure))
            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("BME280 read failed: " + str(e))

    

    

