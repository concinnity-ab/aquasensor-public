import board
import busio
from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import adafruit_bmp280

class bmp280_sensor(generic_sensor):
    '''Class for a Bosch BMP280 T+P sensor; I2C-connected'''

    @classmethod
    def confignames(cls) -> List:
        return ["bmp280"]

    def __init__(self, config:List):# "bmp280" i2caddr tempchannel pressurechannel
        super().__init__(config)
        #default address=0x77 But Adafruit breakout is 0x76(?)
        i2caddr = int(config[1], 16) #Always given in hex.
        if i2caddr > 127 or i2caddr < 0:
            raise ValueError('Invalid I2C address')
        self.tchannel  = config[2]
        self.pchannel  = config[3]
        self.sensor = adafruit_bmp280.Adafruit_BMP280_I2C(busio.I2C(board.SCL, board.SDA), address=i2caddr)
        super().register()

        
    def read(self) -> Dict:
        # read temp+pressure
        try:
            values[self.tchannel] = self.sensor.temperature
            values[self.pchannel] = self.sensor.pressure
            print("BMP280 T {} P {}".format(self.sensor.temperature, self.sensor.pressure))
        except Exception as e:
            self.read_errors += 1
            raise Exception("BMP280 read failed: " + str(e))

    

    

