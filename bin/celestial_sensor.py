from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import ephem
from datetime import datetime, timezone
from math import pi

class celestial_sensor(generic_sensor):
    '''Calculate celestial angles from lat, lon and universal time'''
    
    @classmethod
    def confignames(cls)->List:
        return ["celestial"]


    def solar_angle(self) -> float:
        obs = ephem.Observer()
        obs.lat = self.lat/180*pi #'31:00'
        obs.long = self.lon/180*pi #'-106:00'
        obs.date = datetime.now(timezone.utc)
        #print(obs)

        sun = ephem.Sun(obs)
        sun.compute(obs)
        #print(float(sun.alt))
        #print(str(sun.alt))
        sun_angle = float(sun.alt) / pi * 180 # Convert Radians to degrees
        #print("sun_angle: %f" % sun_angle)
        return sun_angle

    
    def __init__(self, config:List):
        # "celestial" latitude longitude solar-angle-channel
        #all angles in decimal degrees
        super().__init__(config)
        self.lat = float(config[1])
        self.lon = float(config[2])
        self.channel = config[3]
        super().register()

        
    def read(self) -> Dict:
        try:
            return {self.channel: self.solar_angle()}
        except Exception as e:
            self.read_errors += 1
            raise Exception("Celestial computation failed: " + str(e))
    

    

