#!/bin/python3
#
# Measure EC once a minute, on the half-minute, turning on the current
# in alternating directions
#
import threading
import time
import datetime
import RPi.GPIO as GPIO

exit_event = threading.Event()

class concinnity_ec(threading.Thread):

    def __init__(self, gpioPin1:int, gpioPin2:int, measure_cb):
        threading.Thread.__init__(self)
        print("Measuring EC through relays on pins {} and {}".format(gpioPin1, gpioPin2))

        GPIO.setmode(GPIO.BCM) #GPIO numbers are Broadcom pin numbers
        self.threadID = 1
        self.name = "EC-daemon"
        self.callback = measure_cb
        self.gpioPin1 = gpioPin1
        self.gpioPin2 = gpioPin2
        GPIO.setup(gpioPin1, GPIO.OUT)
        GPIO.output(gpioPin1, 0)
        GPIO.setup(gpioPin2, GPIO.OUT)
        GPIO.output(gpioPin2, 0)
        self.count = 0
        self.value = None #Until we read anything
        self.daemon = True #Stop me if main thread stops
        self.start()

        
    def run(self):
        while not exit_event.is_set():
            #sleep until next whole minute plus 30s
            time.sleep((90 - datetime.datetime.now().second) % 60.0)
            
            self.count += 1
            if self.count % 2 == 0: #even
                print(" EC(Positive)")
                GPIO.output(self.gpioPin1, 1)
            else: #odd
                print(" EC(Negative)")
                GPIO.output(self.gpioPin2, 1)
            time.sleep(0.1) #let it settle?
            try:
                #print("%% calling callback " + str(self.callback))
                voltage = self.callback()
                r = 1000.0 #ohms
                vcc = 5.0 #volts (we never measure 5V, since op-amp buffer cannot swing to more than 4.25V
                self.value = (vcc - voltage)/(voltage * r)
            except Exception as e:
                print("%% EC callback raised: " + str(e))
                self.value = None
                voltage = float('nan')
            GPIO.output(self.gpioPin1, 0)
            GPIO.output(self.gpioPin2, 0)
            print("  Raw voltage: {}  Computed EC: {} T: {}".format(voltage, self.value, time.time()))
            time.sleep(1.0) #prevent micronaps

        #Time to stop
        GPIO.output(self.gpioPin1, 0)
        GPIO.output(self.gpioPin2, 0)
        self.name.exit()

        
    def check(self):
        return self.value

    
    #TODO: ctrl-c stops it
    def stop(self):
        #GPIO.cleanup() #Needed in a thread?
        exit_event.set()


