from typing import List
from typing import Dict
from generic_sensor import generic_sensor
from generic_sensor import add_port
from ads1015_sensor import ads1015_sensors
import concinnity_ec

class concinnity_ec_sensor(generic_sensor):

    @classmethod
    def confignames(cls)->List:
        return ["concinnity_ec"]

    sensor = None

    def callback(self) -> float:
        #print("** starting callback, A/Ds available: " + str(ads1015_sensors))
        #Always use first ADS1015
        ad = ads1015_sensors[0]
        #print("** EC callback will use A/D  " + str(ad))
        res = ad.read()
        #print("** AD returning " + str(res))
        #For now, always use Analog channel 2
        #TODO: configurable?

        return res["_A2"]

    
    def __init__(self, config:List):
        if len(ads1015_sensors) == 0:
            raise Exception("No ADS1015 A/D converter defined.")
        
        # "concinnity_ec" pin1 pin2 channelname
        super().__init__(config) #checks+reserves pin 1
        self.pin1 = int(config[1])
        self.pin2 = int(config[2])
        add_port(self.pin2)
        self.channel = config[3]

        self.sensor = concinnity_ec.concinnity_ec(self.pin1, self.pin2, self.callback)
        super().register()


    def read(self) -> Dict:
        '''read the data from the EC probe that was sampled 30s before'''
        result = {}
        ec = self.sensor.check()
        if ec is not None:
            result[self.channel] = ec
        else:
            self.read_errors += 1
            raise Exception("Concinnity EC read failed, no data. ")
        return result

    
    def close(self):
        self.sensor.stop()
        
    

