#!/usr/bin/python3
import serial
import time
import threading
from typing import Dict

class concinnity_klimat(threading.Thread):
    "TODO: teach this class to blink LEDs. Requires keeping state for each LED."
    
    def clr(self):
        self.temp = None
        self.hum = None
        self.illumination = None
        self.co2 = None
        self.pressure = None
        self.error = None

    def __init__(self, ser="/dev/ttyACM0"):
        threading.Thread.__init__(self)
        self.daemon = True #stop me if main thread stops
        self.clr()
        self.latest = time.time()
        self.portname = ser
        self.serial = None
        self.outputBuffer = None
        self.beat = True
        self.start()

    def run(self):
        while True:
            if self.serial == None: #have to open it        
                try:
                    print("Looking for Concinnity Klimatlåda on port " + self.portname)
                    self.serial = serial.Serial(self.portname, 9600, timeout = 5) #TODO: play w this timeout
                    #Success, run a little blinkenlights:
                    """self.serial.write(b"1234") #turn on all LEDs 50%
                    self.serial.flush()
                    time.sleep(1.0)
                    self.serial.write(b"BGYR") #turn on all LEDs full
                    self.serial.flush()
                    time.sleep(1.0)
                    self.serial.write(b"1234") #turn on all LEDs 50%
                    self.serial.flush()
                    time.sleep(1.0)"""
                    time.sleep(5.0) #wait for onboard LED test to run
                    self.serial.write(b"Bgyr") #turn off all LEDs except for power
                    self.serial.flush()
                except Exception as e:
                    print("Failed to open serial port: " + str(e))
                    self.serial = None
                    time.sleep(5.0)
            
            if self.serial != None: #Yay, open
                try:
                    bytes = self.serial.readline()
                    line = bytes.decode('iso8859-1').strip()
                    #print(line) #debug
                    if len(line) == 0 or line[0] == '#': #ignore
                        continue
                    parts = line.split()
                    if len(parts) < 3: #not useful
                        continue
                    self.latest = time.time() 
                    if parts[0] == "T": #temperature, Celsius
                        self.temp = float(parts[2])
                    elif parts[0] == "H": #rel humidity, %
                        self.hum = float(parts[2])
                    elif parts[0] == "I": #Illumination, lux
                        self.illumination = float(parts[2])
                    elif parts[0] == "C": #CO2, ppm
                        self.co2 = float(parts[2])
                    elif parts[0] == "P": #Air pressure, hPa
                        self.pressure = float(parts[2])
                    elif parts[0] == "E": #error
                        self.error = line[2:] 
                    else:
                        self.error = "Unknown data type " + line
                        
                    if self.beat:
                        #print("beat!")
                        self.serial.write(b"G") #turn on green LED
                        self.serial.flush()
                        time.sleep(2.0)
                        self.serial.write(b"g") #turn off green LED
                        self.serial.flush()
                        self.beat = False
                        if self.outputBuffer is not None:
                            self.serial.write(self.outputBuffer.encode('iso8859-1'))
                            self.serial.flush()
                            self.outputBuffer = None
                except Exception as e:
                    print("Failed to read from serial port: " + str(e))
                    self.serial = None

                        
    def get(self) -> Dict:
        #report empty if value obsolete
        age = time.time() - self.latest
        if age > 40.0:
            self.clr()
        self.beat = True
        return {"temp": self.temp, "rh": self.hum, "illumination": self.illumination, "co2": self.co2, "pressure": self.pressure, "error": self.error, "age": age}


    def send(self, output: str):
        #TODO: could get overwritten if called too often
        self.outputBuffer = output
        return
        

def main():
    kli = klimatning()
    while True:
#        try:
         print(kli.get())
#        except:
#        print("err")
         time.sleep(30.0)

if __name__ == '__main__':
    main()
