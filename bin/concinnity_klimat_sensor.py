from typing import List
from typing import Dict
from led_sensor import flag_sensor
import concinnity_klimat

class concinnity_klimat_sensor(flag_sensor):

    @classmethod
    def confignames(cls)->List:
        return ["concinnity_klimat"]

    sensor = None

    def __init__(self, config:List):
        # "concinnity_klimat" serialport tchannel hchannel pchannel ichannel cchannel
        super().__init__(config)
        self.fn = config[1]
        self.tchannel = config[2]
        self.hchannel = config[3]
        self.pchannel = config[4]
        self.ichannel = config[5]
        self.cchannel = config[6]

        self.sensor = concinnity_klimat.concinnity_klimat(self.fn)
        super().register()

        
    def read(self) -> Dict:
        "Sum up the sizes of all the files in the directory in bytes"
        try:
            result = {}
            #read satellite sensors
            kli = self.sensor.get()
            if kli["temp"] is not None:
                result[self.tchannel] = kli["temp"]
            if kli["rh"] is not None:
                result[self.hchannel] = kli["rh"]
            if kli["pressure"] is not None:
                result[self.pchannel] = kli["pressure"]
            if kli["illumination"] is not None:
                result[self.ichannel] = kli["illumination"]
            if kli["co2"] is not None:
                result[self.cchannel] = kli["co2"]

            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("Concinnity Klimatlåda read failed: " + str(e))

        
    def set(blue, red, green, yellow):
        '''each color can be True, False or None(=no change)'''
        ledstr = ""
        if blue is True:
            ledstr += "B"
        if blue is False:
            ledstr += "b"

        if yellow is True:
            ledstr += "Y"
        if yellow is False:
            ledstr += "y"

        if red is True:
            ledstr += "R"
        if red is False:
            ledstr += "r"

        if green is True:
            ledstr += "G"
        if green is False:
            ledstr += "g"

        self.sensor.send(ledstr)

    
    
    

