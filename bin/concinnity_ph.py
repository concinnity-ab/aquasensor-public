#!/usr/bin/python3
import serial
import time
import threading
from typing import Dict

class concinnity_ph(threading.Thread):
    "Receive and store data from a Concinnity pH satellite sensor."
    
    def clr(self):
        self.temp = None
        self.inttemp = None
        self.inthum = None
        self.ph = None
        self.mv = None
        self.error = None

    def __init__(self, ser="/dev/ttyS0"):
        threading.Thread.__init__(self)
        self.daemon = True #stop me if main thread stops
        self.clr()
        self.latest = time.time()
        self.portname = ser
        self.serial = None
        self.outputBuffer = None
        self.beat = True
        self.start()

    def run(self):
        while True:
            if self.serial == None: #have to open it        
                try:
                    print("Looking for Concinnity pH on port " + self.portname)
                    self.serial = serial.Serial(self.portname, 9600, timeout = 5) #TODO: play w this timeout
                    #Success
                except Exception as e:
                    print("Failed to open serial port: " + str(e))
                    self.serial = None
                    time.sleep(5.0)
            
            if self.serial != None: #Yay, open
                try:
                    bytes = self.serial.readline()
                    line = bytes.decode('iso8859-1').strip()
                    #print(line) #debug
                    if len(line) == 0 or line[0] == '#': #ignore
                        continue
                    parts = line.split()
                    if len(parts) < 3: #not useful
                        continue
                    self.latest = time.time() 
                    if parts[0] == "T": #temperature, Celsius
                        if parts[1] == "0":
                            self.inttemp = float(parts[2])
                        if parts[1] == "1":
                            self.temp = float(parts[2])
                    elif parts[0] == "H": #rel humidity, %
                        self.inthum = float(parts[2])
                    elif parts[0] == "V": #raw mV from pH probe
                        self.mv = float(parts[2])
                    elif parts[0] == "P": #pH
                        self.ph = float(parts[2])
                    elif parts[0] == "E": #error
                        self.error = line[2:] 
                    else:
                        self.error = "Unknown data type " + line
                        
                except Exception as e:
                    print("Failed to read from serial port: " + str(e))
                    self.serial = None

                        
    def get(self) -> Dict:
        #report empty if value obsolete
        age = time.time() - self.latest
        if age > 40.0:
            self.clr()
        return {"temp": self.temp, "inttemp": self.inttemp, "inthum": self.inthum, "ph": self.ph, "mv": self.mv, "error": self.error, "age": age}


    def send(self, output: str):
        #TODO: could get overwritten if called too often
        self.outputBuffer = output
        return
        

def main():
    kli = klimatning()
    while True:
        print(kli.get())
        time.sleep(30.0)

if __name__ == '__main__':
    main()
