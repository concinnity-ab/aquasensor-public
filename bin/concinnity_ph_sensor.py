from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import concinnity_ph

class concinnity_ph_sensor(generic_sensor):

    @classmethod
    def confignames(cls)->List:
        return ["concinnity_ph"]

    sensor = None

    def __init__(self, config:List):
        # "concinnity_ph" serialport tchannel hchannel pchannel ichannel cchannel
        super().__init__(config)
        self.serialport = config[1]
        self.tchannel = config[2]
        self.phchannel = config[3]
        self.mvchannel = config[4]
        self.itchannel = config[5]
        self.ihhannel = config[6]

        self.ph = concinnity_ph.concinnity_ph(self.serialport)
        super().register()

    def read(self) -> Dict:
        "Sum up the sizes of all the files in the directory in bytes"
        try:
            result = {}
            #read satellite sensors
            kli = self.ph.get()
            if kli["temp"] is not None:
                result[self.tchannel] = kli["temp"]
            if kli["ph"] is not None:
                result[self.phchannel] = kli["ph"]
            if kli["mv"] is not None:
                result[self.mvchannel] = kli["mv"]
            if kli["inttemp"] is not None:
                result[self.itchannel] = kli["inttemp"]
            if kli["inthum"] is not None:
                result[self.ihhannel] = kli["inthum"]

            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("Concinnity pH read failed: " + str(e))

    
    
    

