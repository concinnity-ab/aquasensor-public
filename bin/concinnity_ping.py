#!/usr/bin/python3
import serial
import time
import threading
from typing import Dict

class concinnity_ping(threading.Thread):
    "Receive and store data from a Concinnity Ping satellite sensor."
    
    def clr(self):
        self.inttemp = None
        self.inthum = None
        self.cm = None
        self.us = None
        self.error = None

    def __init__(self, ser="/dev/ttyS0"):
        threading.Thread.__init__(self)
        self.daemon = True #stop me if main thread stops
        self.clr()
        self.latest = time.time()
        self.portname = ser
        self.serial = None
        self.outputBuffer = None
        self.beat = True
        self.start()

    def run(self):
        while True:
            if self.serial == None: #have to open it        
                try:
                    print("Looking for Concinnity Ping on port " + self.portname)
                    self.serial = serial.Serial(self.portname, 9600, timeout = 15) #TODO: play w this timeout
                    #Success
                except Exception as e:
                    print("Failed to open serial port: " + str(e))
                    self.serial = None
                    time.sleep(5.0)
            
            if self.serial != None: #Yay, open
                try:
                    bytes = self.serial.readline()
                    line = bytes.decode('iso8859-1').strip()
                    #print(line) #debug
                    if len(line) == 0 or line[0] == '#': #ignore
                        continue
                    parts = line.split()
                    if len(parts) < 2: #not useful
                        continue
                    self.latest = time.time() 
                    if parts[0] == "T":
                            self.inttemp = float(parts[2])
                    elif parts[0] == "H": #rel humidity, %
                        self.inthum = float(parts[1])
                    elif parts[0] == "U": #raw µs delay (two-way)
                        self.us = float(parts[1])
                    elif parts[0] == "C": #cm distance
                        self.cm = float(parts[1])
                    elif parts[0] == "E": #error
                        self.error = line[2:] 
                    else:
                        self.error = "Unknown data type " + line
                        
                except Exception as e:
                    print("Failed to read from serial port: " + str(e))
                    self.serial = None

                        
    def get(self) -> Dict:
        #report empty if value obsolete
        age = time.time() - self.latest
        if age > 40.0:
            self.clr()
        return {"inttemp": self.inttemp, "inthum": self.inthum, "cm": self.cm, "us": self.us, "error": self.error, "age": age}


def main():
    kli = concinnity_ping("/dev/ttyAMA0")
    while True:
        print(kli.get())
        time.sleep(30.0)

if __name__ == '__main__':
    main()
