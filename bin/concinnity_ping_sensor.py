from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import concinnity_ping

class concinnity_ping_sensor(generic_sensor):

    @classmethod
    def confignames(cls)->List:
        return ["concinnity_ping"]

    sensor = None

    def __init__(self, config:List):
        # "concinnity_ping" serialport cmchannel uschannel tchannel hchannel
        super().__init__(config)
        self.serialport = config[1]
        self.cmchannel = config[2]
        self.uschannel = config[3]
        self.itchannel = config[4]
        self.ihchannel = config[5]

        self.ping = concinnity_ping.concinnity_ping(self.serialport)
        super().register()

        
    def read(self) -> Dict:
        try:
            result = {}
            #read satellite sensors
            kli = self.ping.get()
            if kli["cm"] is not None:
                result[self.cmchannel] = kli["cm"]
            if kli["us"] is not None:
                result[self.uschannel] = kli["us"]
            if kli["inttemp"] is not None:
                result[self.itchannel] = kli["inttemp"]
            if kli["inthum"] is not None:
                result[self.ihhannel] = kli["inthum"]
            print(" Concinnity Ping result: ", str(result))
            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("Concinnity Ping read failed: " + str(e))

    
    
    

