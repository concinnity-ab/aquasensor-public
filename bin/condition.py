from typing import List, Dict

conditions = []

nextnumber = 1


class condition:
    '''Base class of all alarms'''

    name = ""
    delay = 1 #on first read
    message = ""
    triggered = False
    duration = 0
    reference = 0.0

    @classmethod
    def confignames(cls)->List:
        return ['alarm']


    def __init__(self, config:List):
        '''Remember config; give it a name'''
        #op is ==,<,> or !=
        #Todo add an op to check for [non]existant value

        global conditions, nextnumber

        self.delay = int(config[1]) #cycles
        self.message = config[2]
        self.channel = config[3]
        self.op = config[4]
        self.reference = float(config[5])
        self.name = "C{}".format(nextnumber)
        nextnumber += 1

        if self.op not in ['==','<','>','!=']:
            raise ValueError("Operation {} unknown!".format(self.op))

        conditions.append(self)
        ### to main: add_sensor_class(alarm_condition.alarm_condition) #Does not count as a sensor
    
        
    def check(self, values: Dict, lastValues: Dict) -> bool:
        #Todo Handle delay
        print(' Checking {}'.format(self))
        if self.channel not in values:
            print("Cannot check channel {} for alarm; no value!".format(channel))
            return
        vnow = values[self.channel]
        if vnow is None:
            print("Cannot check channel {} for alarm; value is None!".format(channel))
            return
        vref = self.reference
        match = False
        if self.op == '==':
            match = vnow == vref
        elif self.op == '<':
            match = vnow < vref
        elif self.op == '>':
            match = vnow > vref
        elif self.op == '!=':
            match = vnow != vref

        if match is False:
            self.duration = 0
            self.triggered = False
            return None
        #We have a fulfilled condition
        self.duration += 1
        msg = "{} {} is {} ({} {}) for {} cycles".format(
            self.message, self.channel, vnow, self.op, vref, self.duration)
        #print(msg)

        if self.duration >= self.delay and not self.triggered: #Only once until reset
            self.triggered = True
            return msg

        
    def get_name(self):
        return self.name

    
    def get_type(self):
        return 'alarm'
    

    def get_errors(self):
        return 0

    
    def __str__(self):
        return "{}({} {} {})".format(self.name, self.channel, self.op, self.reference)
