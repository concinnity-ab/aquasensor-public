from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import os

class cycles_sensor(generic_sensor):

    @classmethod
    def confignames(cls)->List:
        return ["cycles", "cycles_persistent"]

    count = 0
    persistent = False

    def save(self):
        #try to write stored value to file
        #TODO: only store sometimes, like every 10th or 60th?
        #print("Saving cycle count to ", self.filename)
        try:
            with open(self.filename, mode="w") as f: #create or truncate
                f.write(str(self.count) + "\n")
        except Exception as e:
            print(" Could not store persistent cycle count: ", str(e))
        
    
    def __init__(self, config:List):
        # "cycles" "-" channel
        # "cycles_persistent" filename channel
        super().__init__(config)
        self.channel = config[2]
        if config[0] == "cycles_persistent":
            self.persistent = True
            self.filename = config[1]
            print("Restoring cycle count from ", self.filename)
            #try to read stored value from file
            if os.path.exists(self.filename):
                try:
                    with open(self.filename, mode="r") as f:
                        lines = f.readlines()
                    self.count = int(lines[0])
                except Exception as e:
                    print(" Could not restore persistent cycle count: ", str(e))


        
    def read(self) -> Dict:
        self.count += 1
        #print(self.name, " incrementing cycles. Persistent:  ", self.persistent)
        if self.persistent:
            self.save()
        return {self.channel: self.count}

    

