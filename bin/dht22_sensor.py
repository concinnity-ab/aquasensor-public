import board
import busio
import time
from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import Adafruit_DHT

class dht22_sensor(generic_sensor):
    '''Class for an DHT22 T+RH sensor; proprietary 1-pin protocol
    deprecated - too unreliable data transfer on a Pi
    '''

    @classmethod
    def confignames(cls) -> List:
        return ["dht22"]

    def __init__(self, config:List):# "dht22" gpio-pin tempchannel humchannel
        super().__init__(config)
        self.pin = config[1]
        self.tchannel  = config[2]
        self.hchannel  = config[3]
        self.sensortype = Adafruit_DHT.DHT22
        super().register()

        
    def read(self) -> Dict:
        result = {}
        try:
            hum1, temp1 = Adafruit_DHT.read(self.sensortype, self.pin)
            if hum1 is not None:
                result[self.hchannel] = hum1
            if temp1 is not None:
                result[self.tchannel] = temp1

            if hum1 is None or temp1 is None: #Picky timing makes it flaky on RPi - often fails to read
                time.sleep(3.0)        #retry once after required delay (>=2s)
                hum2, temp2 = Adafruit_DHT.read(self.sensortype, self.pin)
                if hum2 is not None:
                    result[self.hchannel] = hum2
                if temp2 is not None:
                    result[self.tchannel] = temp2
                
            #print("DHT22 T {} H {}".format(self.sensor.temperature, self.sensor.relative_humidity))
            if hum1 is None and hum2 is None or temp1 is None and temp2 is None:
                self.read_errors += 1

            return result
        
        except Exception as e:
            self.read_errors += 1
            raise Exception("DHT22 read failed: " + str(e))
    
