from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import os

class dirsize_sensor(generic_sensor):

    @classmethod
    def confignames(cls)->List:
        return ["dirsize"]

    def __init__(self, config:List):
        # "dirsize" filename sizechannel
        super().__init__(config)
        self.fn = config[1]
        self.sizechannel = config[2] #mandatory
        super().register()

        
    def read(self) -> Dict:
        "Sum up the sizes of all the files in the directory in bytes"
        try:
            size = 0
            for r,d,f in os.walk(self.fn):
                for item in f:
                    stat = os.stat(r + "/" + item)
                    size += stat.st_size
            result = {}
            result[self.sizechannel] = size
            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("Dir size read failed: " + str(e))
    

    

