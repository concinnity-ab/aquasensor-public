from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import pigpio

class dust_sensor(generic_sensor):

    @classmethod
    def confignames(cls) -> List:
        return ["dust"]

    # use pigpio for timing pulses
    pig = None
    g_tick = None
    dust_total_us = 0

    
    def dust_callback(self, g, lvl, t):
     if lvl == 0: #falling edge: pulse begins
        self.g_tick = t
     else: #rising edge: pulse done
        if lvl == 1 and self.g_tick is not None:
           downtime = pigpio.tickDiff(self.g_tick, t)
           #print("Dust pulse µs: ", downtime)
           self.dust_total_us = self.dust_total_us + downtime

           
    def __init__(self, config:List):# "dust" pin channel-name
        super().__init__(config)
        pin = int(config[1])
        self.channel = config[2]
        self.pig = pigpio.pi()  # Connect to local Pi.
        self.pig.callback(pin, pigpio.EITHER_EDGE, self.dust_callback)
        super().register()


    def read(self) -> Dict:
        '''Read concentration from dust sensor (via PIGPIO)'''
        try: #magic formula
            percentage = 100.0 * self.dust_total_us / 60000000.0 #microseconds in a minute
            concentration = 1.1 * percentage ** 3 - 3.8 * percentage ** 2 + 520 * percentage + 0.62
            self.dust_total_us = 0
            #print(self.name, '  Dust concentation: ', concentration)
            return {self.channel:concentration}
        except:
            errors = errors + 1
        return None

    

    

