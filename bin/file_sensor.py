from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import os

class file_sensor(generic_sensor):
    '''Read data from (first) line in a text file'''
    #TODO: a variant that returns 0 if file could not be found/read
    
    @classmethod
    def confignames(cls)->List:
        return ["file"]


    def read_from_textfile(self, fn: str) -> float:
            with open(fn, mode="r") as f:
                lines = f.readlines()
            return float(lines[0])

        
    def __init__(self, config:List):
        # "file" filename sizechannel
        super().__init__(config)
        self.fn = config[1]
        self.channel = config[2]
        #check existence and readablity
        #  (possibly too harsh? Might be useful even if only exists sporadically?)
        with open(self.fn, mode="r") as f:
            pass #exception if not allowed
        super().register()

        
    def read(self) -> Dict:
        try:
            return {self.channel: self.read_from_textfile(self.fn)}
        except Exception as e:
            self.read_errors += 1
            raise Exception("File read failed: " + str(e))
    

    

