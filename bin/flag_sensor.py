from typing import List
from typing import Dict
from generic_sensor import generic_sensor

flag_sensors = []

class flag_sensor(generic_sensor):
    '''A sensor that also has one or more "flags", such as lights'''
    @classmethod
    def confignames(cls)->List:
        return ["flag_sensor"]

    sensor = None

    def __init__(self, config:List):
        global led_sensors
        super().__init__(config)
        flag_sensors.append(self)
        super().register()

        
    def read(self) -> Dict:
        super().read()

        
    def set(blue, red, green, yellow):
        '''each color can be True, False or None(=no change)'''
        Print('set() method not overridden!')
    
