from typing import List

nextname = "A"

dummy_port = "-"

used_ports = []

sensors = []


def set_used_ports(ports: List):
    '''Called to set up the ports that are in use before we create any sensor objects'''
    global used_ports
    used_ports = ports

    
def get_used_ports() -> List:
    '''Called to get the ports that are in use'''
    #global used_ports
    return used_ports

    
def add_port(port):
    '''Called to reserve a port, other than that given in config[1]'''
    global used_ports
    if port is not dummy_port:
        if port in used_ports:
            raise ValueError("Port {} already in use!".format(port))
        used_ports.append(port)

        
class generic_sensor:
    '''Base class of all sensors'''

    read_errors = 0
    config_errors = 0
    name = ''
    sensortype = ''


    @classmethod
    def confignames(cls)->List:
        return [] #abstract, no config name(s)


    def __init__(self, config:List):
        '''Check that port is free, reserve it and name this sensor'''
        global nextname, used_ports
        add_port(config[1])
        self.sensortype = config[0]
        self.name = nextname + "(" + config[0] + ")"
        nextname = chr(ord(nextname) + 1)


    def register(self):
        '''If we do this in  _init__() it is polled whether it failed in subclass __init__ or not'''
        sensors.append(self)

        
    def read(self):
        Print('Read method not overridden!')
        return None


    def get_name(self):
        return self.name

    
    def get_errors(self):
        return (self.read_errors, self.config_errors)


    def get_type(self):
        return self.sensortype


    def close(self):
        #overide for any necessary cleanup, like closing files
        return


    def __str__(self):
        return "Sensor {}({})".format(self.name, self.sensortype)
