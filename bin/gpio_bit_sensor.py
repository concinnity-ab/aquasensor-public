from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import RPi.GPIO as GPIO

class gpio_bit_sensor(generic_sensor):

    @classmethod
    def confignames(cls) -> List:
        return ['gpio_bit', 'gpio_bit_pulldown', 'gpio_bit_pullup']


    def __init__(self, config:List):# "gpio_bit*" pin channel-name
        super().__init__(config)
        self.pin = int(config[1]) #todo: check for sanity (2-32)
        self.channel = config[2]

        if config[0] == "gpio_bit":
            GPIO.setup(self.pin, GPIO.IN)
        elif config[0] == "gpio_bit_pulldown":
            GPIO.setup(self.pin, GPIO.IN, pull_up_down = GPIO.PUD_DOWN)            
        elif config[0] == "gpio_bit_pullup":
            GPIO.setup(self.pin, GPIO.IN, pull_up_down = GPIO.PUD_UP)
        super().register()

        
    def read(self) -> Dict:
        return {self.channel: GPIO.input(self.pin)}

    

