from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import RPi.GPIO as GPIO

class gpio_counter_sensor(generic_sensor):

    @classmethod
    def confignames(cls) -> List:
        return ["gpio_counter"]

    
    def gpio_rise_callback(pin: int):
        if self.pin == pin:
            self.count += 1

            
    def __init__(self, config:List):# "gpio_counter" pin channel-name
        super().__init__(config)
        self.pin = int(config[1]) #todo: sheck for sanity (2-32)
        self.channel = config[2]
        GPIO.setup(self.pin, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) #always, so far
        self.count = 0
        GPIO.add_event_detect(self.pin, GPIO.RISING)
        GPIO.add_event_callback(self.pin, gpio_rise_callback)
        super().register()

        
    def read(self) -> Dict:
        return {self.channel:self.count}

    

    

