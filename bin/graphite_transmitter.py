from typing import List, Dict
from transmitter import transmitter
import time
import socket

class graphite_transmitter(transmitter):
    '''Graphite/Grafana data transmitter'''

    server = None
    port = 0
    prefix = ""
    
    @classmethod
    def confignames(cls)->List:
        return ['graphite']


    def __init__(self, config:List):
        '''Remember config:
        graphite 12003 127.0.0.1 johannas     
        '''

        super().__init__(config)
        self.port = int(config[1])
        self.server = config[2]
        self.prefix_pattern = config[3]
    

    def send_graphite_value(self, socket, prefix, name: str, value, t:int) -> int:
        '''Send one value IF good to a Graphite server; return bytes used'''
        if value is None:
            return 0
        if type(value) is float: #reduce number of decimals
            buf = ("{}{} {:.3f} {}\n".format(prefix, name, value, t)).encode("utf8")
        else: #int or bool
            buf = ("{}{} {} {}\n".format(prefix, name, value, t)).encode("utf8")
        print(buf) #debug
        socket.sendall(buf)
        return len(buf)


    def send(self, values: Dict) -> int:
        '''Send all values to a graphite server'''

        print("Sending to Graphite server at " + self.server + ":" + str(self.port))
        total_bytes = 0
        t = int(time.time()) #same time for all values

        #connect to db. May fail if server (Grafsy if local) is down.
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((self.server, self.port))
        except Exception as e:
            print("Could not connect to server:" + str(e))
            return

        pfx = self.prefix_pattern.format(socket.gethostname())
        for k, v in values.items():
            total_bytes += self.send_graphite_value(s, pfx, k, v, t) 

        s.close()

        print("{} bytes sent".format(total_bytes))
        return(total_bytes)
