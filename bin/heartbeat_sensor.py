import socket
from typing import List
from typing import Dict
from generic_sensor import generic_sensor

class heartbeat_sensor(generic_sensor):
    '''Class for a pseudosensor that sends a network "heartbeat".
    Currently sends nothing to Graphite '''

    @classmethod
    def confignames(cls) -> List:
        return ["heartbeat", "heartbeat_tcp"]

    def __init__(self, config:List):# "heartbeat" server:port string
        super().__init__(config)
        nodeport = config[1].split(":")
        if len(nodeport) != 2:
            raise ValueError('Invalid node:port address')
        self.port = int(nodeport[1])
        self.server = nodeport[0]
        self.message = config[2]
        self.udp = (config[0] == "heartbeat")
        super().register()


    def read(self) -> Dict:
        buf = (self.message).format(socket.gethostname()).encode("utf8")
        try:
            if self.udp: #UDP (KISS principle)
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                s.sendto(buf, (self.server, self.port))
            else: #TCP
                #connect to target.
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.connect((self.server, self.port))
                s.sendall(buf)
                s.close()

        except Exception as e:
            print("Could not send to {}:{} :{}".format(self.server, self.port, e))
            self.read_errors += 1

        return {} #Or should we return a true/false bit channel?
    


    

