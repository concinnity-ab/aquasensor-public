from typing import List
from typing import Dict
from generic_sensor import add_port
from flag_sensor import flag_sensor

led_sensors = []

class led_sensor(flag_sensor):
    '''A sensor (or not) that also has one or more LEDs'''

    @classmethod
    def confignames(cls)->List:
        return ["leds"]

    sensor = None

    def __init__(self, config:List):
        global led_sensors
        super().__init__(config)
        led_sensors.append(self)
        self.leds = []
        for c in config[1:]:
            add_port(c)
            self.leds.append[c]
        super().register()

        
    def read(self) -> Dict:
        return {} #pure indicator

    
    def set(blue, red, green, yellow):
        '''each color can be True, False or None(=no change)'''
        Print('set() method not overridden!')
    
