from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import MHZ14A

class mhz14a_sensor(generic_sensor):

    @classmethod
    def confignames(cls)->List:
        return ["mhz14a", "mhz14a_abc"]

    sensor = None

    def __init__(self, config:List):
        # "mhz14a" serial-port co2-channel [tempchannel]
        # "mhz14a_abc" serial-port co2-channel [tempchannel]
        super().__init__(config)
        enable_abc = (config[0] == "mhz14a_abc")
        fn = config[1]
        self.co2channel = config[2] #mandatory
        if len(config) > 3:
            self.tempchannel = config[3] #optional
        else:
            self.tempchannel = None

        self.sensor = MHZ14A.MHZ14A(fn, enable_abc)
        #todo: count any config error
        super().register()

        
    def read(self) -> Dict:
        #read CO2 + T
        try:
            result = {}
            data = self.sensor.get()
            print(" MHZ14A {}: {}".format(self.name, data))
            result = {self.co2channel:data['ppm']}
            if self.tempchannel is not None:
                result[self.tempchannel] = data['temp']
            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("MHZ14A read failed: " + str(e))

        
    def close(self):
        self.sensor.close()

    

    

