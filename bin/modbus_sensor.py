from typing import List
from typing import Dict
from generic_sensor import generic_sensor
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder

class modbus_sensor(generic_sensor):
    '''Class for reading MODBUS (32-bit float values only for now)
    More than one of these 'sensors' can be configured on the same serial port but several channels on the same sensor would be more elegant
    '''

    channels = {}

    @classmethod
    def confignames(cls) -> List:
        return ["modbus"]

    
    def __init__(self, config:List):
        '''
        Currently:
        "modbus" serialport unit register channelname
        Later:
        "modbus_rtu" serialport (datatype unit#  register# channelname)*
        "modbus_tcp" -          (datatype ipaddr register# channelname)*
        '''
        super().__init__(config) #gets added if port OK even if it fails later!
        self.serialport = config[1]
        for c in range(1,(len(config) - 2) // 4):
            self.channels[config[c*4 + 1]] = (config[c*4 - 2],config[c*4 - 1 ],config[c*4])
        print(self.channels) #debug
        self.unit = int(config[3])
        self.register = int(config[4])
        self.channel = config[5]
        self.client = ModbusClient(method='rtu', port=self.serialport, timeout=1, baudrate=9600)
        assert(self.client)
        assert(self.client.connect()) #TODO: can it becomme disconnected? Do we need to check and reconnect before every read? Is the serial port locked?
        super().register()

        
    def read(self) -> Dict:
        result = {}
        try:
            result[self.channel] = self.read_float32(self.register)
            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("MODBUS read failed: " + str(e))

        
    def read_float32(reg:int) -> float:
        '''read a 32-float from 2 registers, low word first'''

        rr = self.client.read_input_registers(self.register, 2, unit=self.unit) #2 regs for a float
        assert(not rr.isError())
        print(" MODBUS data: {}".format(rr.registers))
        decoder = BinaryPayloadDecoder.fromRegisters(rr.registers, Endian.Big, wordorder=Endian.Little)
        f = decoder.decode_32bit_float()
        print(" MODBUS float: {}".format(f))
        
