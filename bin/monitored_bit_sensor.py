from generic_sensor import generic_sensor
from typing import List
from typing import Dict
import pigpio

#import time
#import RPi.GPIO as GPIO

class monitored_bit_sensor(generic_sensor):
    '''Class for a GPIO bit that needs to be monitored continously'''

    @classmethod
    def confignames(cls) -> List:
        return ["monitored_bit", "monitored_bit_pulldown", "monitored_bit_pullup"]

    pig = None
    g_tick = None
    high_pulses = 0
    high_total_pulses = 0
    high_total_us = 0
    low_pulses = 0
    low_total_pulses = 0
    low_total_us = 0

    
    def edge_callback(self, g, lvl, t):
        if self.g_tick is not None:
            delta = pigpio.tickDiff(self.g_tick, t)
            if lvl == 0: #falling edge: low pulse begins, high pulse ends
                self.high_pulses += 1
                self.high_total_pulses += 1
                self.high_total_us += delta
                print(" Bit Up ", delta)
            else: #rising edge: low pulse ends, high pulse begins
                self.low_pulses += 1
                self.low_total_pulses += 1
                self.low_total_us += delta
                print(" Bit Dn ", delta)

        self.g_tick = t
        return

    
    def __init__(self, config:List):# "monitored_bit_*" gpio-pin uptime-s-channel downtime-s-channel delta-count-up-channel total-count-up-channel 
        super().__init__(config)
        
        self.port = int(config[1])
        self.uptime_channel  = config[2]
        self.downtime_channel = config[3]
        self.dcount_up_channel  = config[4]
        self.tcount_up_channel  = config[5]

        self.pig = pigpio.pi()  # Connect to local Pi.
        self.pig.set_mode(self.port, pigpio.INPUT) #just in case
        
        if config[0] == "monitored_bit_pullup":
            self.pig.set_pull_up_down(self.port, pigpio.PUD_UP)
        elif config[0] == "monitored_bit_pulldown":
            self.pig.set_pull_up_down(self.port, pigpio.PUD_DOWN)
        else:
            self.pig.set_pull_up_down(self.port, pigpio.PUD_OFF)

        self.pig.set_glitch_filter(self.port, 10000) #10ms. good?

        self.pig.callback(self.port, pigpio.EITHER_EDGE, self.edge_callback)

        print("Monitoring pin " + str(self.port))
        super().register()


    def read(self) -> Dict:
        result = {}
        result[self.downtime_channel] = self.low_total_us/1000000
        result[self.uptime_channel] = self.high_total_us/1000000
        result[self.dcount_up_channel] = self.high_pulses
        result[self.tcount_up_channel] = self.high_total_pulses
        #if self.low_total_us == 0:
        #    result[self.all_up_channel] = 1
        #else:
        #    result[self.all_up_channel] = 0
        #symmetry?

        #reset for next sample cycle
        self.high_total_us = 0
        self.low_total_us = 0
        self.high_pulses = 0
        self.low_pulses = 0

        return result

