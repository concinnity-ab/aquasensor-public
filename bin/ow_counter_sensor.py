from typing import List
from generic_sensor import generic_sensor

class ow_counter(generic_sensor):
    '''Class for OneWire counters, DS1824, that appear in the file system
    like /sys/bus/w1/devices/1d-0000000cf798/w1_slave'''

    port = ''
    channel_a = None
    channel_b = None
    last_a = 0
    last_b = 0

    @classmethod
    def confignames(cls)->List:
        return ["ow_counters"]
    

    def __init__(self, config:List):
        super().__init__(config)
        self.port = config[1]
        self.channel_a = config[2]
        if len(config) > 3:
            self.channel_b = config[3]
        super().register()

    
    def read(self):
        '''Read data from m.nu OneWire counters in the file system'''
        #4 lines, all like this:
        #  00 2d 00 00 00 00 00 00 00 8f ed 00 ff fe 00 00 ff ff 00 00 ff ff 00 00 ff ff 00 00 ff ff 00 08 ff ff 00 00 ff ff 00 00 ff ff crc=YES c=45
        #third line is counter A
        #fourth line is counter B
        result = {}
        try:
            with open(self.port, mode="r") as f:
                lines = f.readlines()
            #print(" Counter Lines: ", lines)

            if len(lines) > 3 and lines[2].find("crc=YES") > -1: #OK
                tpos = lines[2].find(" c=")
                if tpos > -1:
                    self.last_a  = int(lines[2].strip()[tpos+3:])
                    result[self.channel_a] = self.last_a
                    

            if self.channel_b is not None and len(lines) > 3 and lines[3].find("crc=YES") > -1: #OK
                tpos = lines[3].find(" c=")
                if tpos > -1:
                    self.last_b = int(lines[3].strip()[tpos+3:])
                    result[self.channel_b] = self.last_b

        except:
            self.read_errors += 1
            return None

        return result
