from typing import List
from generic_sensor import generic_sensor

class ow_temp(generic_sensor):
    '''Class for OneWire temperature sensors, like DS18B20, that appear in the file system
    like /sys/bus/w1/devices/28-0118339174ff/w1_slave'''

    port = ''

    @classmethod
    def confignames(cls)->List:
        return ["ow_temp"]

    def __init__(self, config:List):
        '''ow_temp devicefile channelname'''
        super().__init__(config)
        self.port = config[1]
        self.channel = config[2]
        super().register() #done last (if everything succeeds)
    
    def read(self):
        #2 lines, like this: 
        #  5e 01 4b 46 7f ff 0c 10 51 : crc=51 YES
        #  5e 01 4b 46 7f ff 0c 10 51 t=21875
        #  T is in millicelsius
        
        try:
            with open(self.port, mode="r") as f:
                lines = f.readlines()
            if len(lines) > 1 and lines[0].strip()[-3:] == "YES":
                tpos = lines[1].find("t=")
                if tpos > -1:
                    return {self.channel:float(lines[1].strip()[tpos+2:]) / 1000.0}

            self.read_errors += 1
            return None
        except:
            self.read_errors += 1
            return None

