from typing import List
from typing import Dict
from alarm_sensor import alarm_sensor
import pagerduty_events_api
import psutil

class pagerduty_sensor(alarm_sensor):
    '''A "sensor" that can send Pagerduty alarms via TCP API
    using PagerDuty library (which has to be installed).'''

    pd_incident = None
    channel = None
    
    @classmethod
    def confignames(cls)->List:
        return ["pagerduty"]

    
    def __init__(self, config:List):
        ''' pagerduty apikey [connection-channel]
        TODO have a modem filename in case of several USB devices? (why?!)''' 
        super().__init__(config)

        service_key = config[1]
        if len(config) > 2:
            self.channel = config[2]

        self.pd_service = pagerduty_events_api.PagerdutyService(service_key)
        super().register()

        
    def read(self) -> Dict:
        #check Pagerduty conection status?
        #For now, just check network is up
        result = {}
        if self.channel is not None:
            try:
                netinfo = psutil.net_if_stats()
                result[self.channel] = netinfo['eth0'].isup
            except Exception as e:
                self.read_errors += 1
                print(" Pagerduty connection check error: ", str(e))
        return result

    
    def close(self):
        #TODO?
        pass


    def trig(self, hostname: str, message: str):
        super().trig(hostname, message)

        if self.pd_incident is not None: #PagerDuty knows(?)
            return

        print(" Sending Pagerduty incident trigger: '" + message + "'")
        try:
            self.pd_incident = self.pd_service.trigger(hostname + ': ' + message)
            print("  incident: ", self.pd_incident)
        except Exception as e:
            print("Pagerduty trigger failed: " + str(e))
            self.pd_incident = None
            self.send_errors += 1

            
    def clear(self, hostname:str, message:str):
        if self.pd_incident is None:
            print(" No Pagerduty incident to resolve: " + message)
            return

        print(" Resolving Pagerduty incident: " + message)
        try:
            self.pd_incident.resolve({'description': message})
        except Exception as e:
            print("Pagerduty resolution failed: " + str(e))
            self.pd_incident = None #???
            self.send_errors += 1
    
