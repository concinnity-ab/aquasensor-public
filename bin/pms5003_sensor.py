from typing import List
from typing import Dict
from generic_sensor import generic_sensor, add_port
from pms5003 import PMS5003

class pms5003_sensor(generic_sensor):

    @classmethod
    def confignames(cls)->List:
        return ["pms5003"] #maybe add active/passive choice?

    sensor = None

    def __init__(self, config:List):
        # "pms5003" serial-port enablepin resetpin pm1channel [pm2.5channel] 
        super().__init__(config)
        fn = config[1]
        add_port(config[2])
        add_port(config[3])
        self.partchannel = config[4]
        if len(config) > 5:
            self.tempchannel = config[5] #optional
        else:
            self.tempchannel = None

        self.sensor = PMS5003(device=fn, baudrate=9600,
                              pin_enable=22,
                              pin_reset=27)
        super().register()


    def read(self) -> Dict:
        #read particle count?
        try:
            result = {}
            data = self.sensor.read()
            print(" PMS5003 {}:\n{}".format(self.name, data))
            result = {self.partchannel:data.pm_per_1l_air(0.3)}
            if self.tempchannel is not None:
                result[self.tempchannel] = data['temp']
            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("PMS5003 read failed: " + str(e))


    

    

