#!/bin/python3
#
# Checks the power-good GPIO about once a second
# and counts how many times it was up and down
#
import threading
import time
import RPi.GPIO as GPIO

exit_event = threading.Event()

class power_monitor(threading.Thread):

    def __init__(self, gpioPin:int):
        threading.Thread.__init__(self)
        print("Monitoring power on pin " + str(gpioPin))

        GPIO.setmode(GPIO.BCM) #GPIO numbers are Broadcom pin numbers
        self.threadID = 1
        self.name = "power monitor"
        self.gpioPin = gpioPin
        self.upCount = 0
        self.dnCount = 0
        self.daemon = True #Stop me if main thread stops
        self.start()
        
    def run(self):
        while not exit_event.is_set():
            if GPIO.input(self.gpioPin):
                #print("Up")
                self.upCount += 1
            else:
                #print("Dn")
                self.dnCount += 1
            time.sleep(1.0)
        self.name.exit()
        
    #atomically reset and return counts
    def check_and_clear(self):
        result = (self.upCount, self.dnCount)
        self.upCount = 0
        self.dnCount = 0
        return result
        
    def check(self):
        return (self.upCount, self.dnCount)

    #TODO: ctrl-c stops it
    def stop(self):
        exit_event.set()


