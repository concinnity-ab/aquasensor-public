from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import RPi.GPIO as GPIO
import power_monitor

power_sensors = []
power_channels = []

class power_sensor(generic_sensor):

    dncycles = 0
    upcycles = 0
    
    @classmethod
    def confignames(cls) -> List:
        return ["power"]

    
    def __init__(self, config:List):# "power" pin ok-channel upseconds-ch dnseconds-ch dncycles
        super().__init__(config)
        self.pin = int(config[1]) #todo: sheck for sanity (2-32)
        self.ok_channel = config[2]
        self.dncycles_channel = config[3]
        self.uptime_channel = config[4]
        self.dntime_channel = config[5]
        GPIO.setup(self.pin, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) #always, so far
        self.sensor = power_monitor.power_monitor(self.pin)

        global power_sensors, power_channels
        power_sensors.append(self)
        power_channels.append(self.ok_channel)
        super().register()

        
    def read(self) -> Dict:
        result = {}
        #Check power history for this cycle    
        up,down = self.sensor.check_and_clear() # [0] seconds of good; [1] seconds of bad power since last clear
        ok = down is 0 #no dropouts means OK
        if ok:
            self.dncycles = 0
            self.upcycles += 1
        else:
            self.upcycles = 0
            self.dncycles += 1

        result[self.ok_channel] = int(ok)
        result[self.uptime_channel] = up
        result[self.dntime_channel] = down
        result[self.dncycles_channel] = self.dncycles
        
        return result

    
    def close(self):
        self.sensor.stop()

    

