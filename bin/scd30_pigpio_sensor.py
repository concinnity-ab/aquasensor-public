from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import pigpio
import scd30 #by laurenzgamper

class scd30_sensor(generic_sensor):
    '''Class for a Sensirion SCD30 CO2+T+H sensor, using the pigpiod library and demon (which must be running!)'''

    @classmethod
    def confignames(cls) -> List:
        return ["scd30", "scd30_abc"]

    
    def __init__(self, config:List):# "scd30" i2caddr co2-channel tempchannel humchannel
        super().__init__(config)
        enable_abc = (config[0] == "scd30_abc")
        i2caddr = int(config[1], 16) #Always given in hex.
        if i2caddr > 127 or i2caddr < 0:
            raise ValueError('Invalid I2C address')
        self.co2channel  = config[2]
        self.tempchannel = config[3]
        self.humchannel  = config[4]
        
        self.sensor = scd30.SCD30(pigpioHost='localhost', i2cSlave=i2caddr, i2cBus=1)
        # trigger continous measurement
        self.sensor.sendCommand(scd30.COMMAND_CONTINUOUS_MEASUREMENT, 970) #milliseconds (?)
        # enable/disable autocalibration
        if enable_abc:
            self.sensor.sendCommand(scd30.COMMAND_AUTOMATIC_SELF_CALIBRATION, 1)
        else:
            self.sensor.sendCommand(scd30.COMMAND_AUTOMATIC_SELF_CALIBRATION, 0)
        super().register()

        
    def read(self) -> Dict:
        #read CO2 + T + RH
        try:
            self.sensor.waitForDataReady()
            data = self.sensor.readMeasurement() #might return False
            if not data:
                raise Exception("No data")
            print(" SCD30 {}: {}".format(self.name, data))
            #[float_CO2, float_T, float_rH] = data
            #values[c[2]] = float_CO2
            #values[c[3]] = float_T
            #values[c[4]] = float_rH
            return {self.co2channel:data[0], self.tempchannel:data[1], self.humchannel:data[2]}
        except Exception as e:
            self.read_errors += 1
            raise Exception("SCD30 (pigpio) read failed: " + str(e))

    

    

