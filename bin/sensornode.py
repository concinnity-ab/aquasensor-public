#!/usr/bin/python3
import sys
import socket
import time
import datetime
import signal
from typing import Dict
from shlex import split as shlexsplit

import RPi.GPIO as GPIO
import board
import busio

#By Concinnity:
from led_sensor import led_sensors
import generic_sensor
from generic_sensor import set_used_ports, get_used_ports, sensors
import heartbeat_sensor
import dust_pigpio_sensor
import scd30_pigpio_sensor
import ow_temp_sensor
import ow_counter_sensor
import mhz14a_sensor
import tsl2561_sensor
import am2320_sensor
import si7021_sensor
import bmp280_sensor
import bme280_sensor
import ads1015_sensor
import dht22_sensor #deprecated
import gpio_bit_sensor
import monitored_bit_sensor
import gpio_counter_sensor
import dirsize_sensor
import power_sensor
import concinnity_klimat_sensor
import concinnity_ph_sensor
import concinnity_ping_sensor
import concinnity_ec_sensor
import pms5003_sensor
import file_sensor
import cycles_sensor
import celestial_sensor
import alarm_sensor
import sms_sensor
import pagerduty_sensor
import silent_alarm_sensor
import modbus_sensor

import condition

from transmitter import transmitters
import graphite_transmitter

#Constants:
CYCLE_SECONDS = 60.0
OVERRUN_ERR_CH = "errors.overrun"
SAMPLING_ERR_CH = "errors.sampling"
DERIVATION_ERR_CH = "errors.derivation"


#Sample all sensors    
def sample(values: Dict):
    global counters, errors, power_channel
    print("% Sampling {} sensors".format(len(generic_sensor.sensors)))

    for sensor in generic_sensor.sensors:
        try:
            newvalues = sensor.read() #one or more channels
            values.update(newvalues)
        except Exception as e:
            errors = errors + 1
            print(sensor.get_name(), " read error: ", str(e))

    print(" {} sampling errors".format(errors))


def derive(values: Dict, last_values: Dict):
    global derivations, derivation_errors
    print("% Calculating {} derived values".format(len(derivations)))

    errors = 0
    for c in derivations:
        t = c[0]
        try:
            if t == "linear": #linear src-channel factor constant target-channel
                #print(c)
                #simple linear z= kx+m transform
                values[c[4]] = values[c[1]] * c[2] + c[3]
            if t == "lowpass": #lowpass src-channel factor target-channel
                print(c)
                #simple digital low-pass filter z = k z0 + (1-k)x
                k = c[2]
                values[c[3]] = k * last_values[c[3]] + (1.0 - k) * values[c[1]]
            if t == "threshold": #threshold src-channel limit bvalue avalue targetchannel
                print(c)
                if values[c[1]] < c[2]:
                    values[c[5]] = c[3]
                else:
                    values[c[5]] = c[4]
        except Exception as e:
            print(" Derivation failed: " + str(e))
            errors += 1

    # Second pass; for derivations depending on two channels, so that config file order is not important
    for c in derivations:
        t = c[0]
        try:
            if t == "compensate": #compensate src-channel factor constant comp-channel
                print(c)
                #simple linear z = x+ky transform
                values[c[1]] = values[c[1]] + c[2] * (values[c[4]] - c[3])
        except Exception as e:
            print(" Derivation failed: " + str(e))
            errors += 1
    
    derivation_errors += errors


#TODO: set led_sensor values even if node has fewer LEDs
def update_leds():
    global values, leds, sampling, alarming, problematic, power_channel
    blue = None
    yellow = None
    red = None
    green = None

    #TODO???
    if power_channel in values:
        if values[power_channel] == True:
            GPIO.output(leds[0], 1) #Power LED
            blue = True
        else:
            GPIO.output(leds[0], 0) #Power LED
            blue = False
    if len(leds) > 1:
        if sampling:
            GPIO.output(leds[1], 1) #Sampling/heartbeat LED
        else:
            GPIO.output(leds[1], 0) #Sampling/heartbeat LED

    if len(leds) > 2:
        if problematic:
            GPIO.output(leds[2], 1) #Problem LED
            yellow = True
        else:
            GPIO.output(leds[2], 0) #Problem LED
            yellow = False

    if len(leds) > 3:
        if alarming:
            GPIO.output(leds[3], 1) #Alarm LED
            red = True
        else:
            GPIO.output(leds[3], 0) #Alarm LED
            red = False

    for indicators in led_sensors:
        indicators.set(blue, red, yellow, green)
       
    
def check_for_alarm_conditions(values: Dict, last_values: Dict):
    global errors, sample_errors, alarming, problematic
    print("% Checking for alarms")
    alarming = False

    for c in condition.conditions:
        message = c.check(values, last_values)
        if message is not None:
            print(" Sending alarms via {}".format(str(alarm_sensor.alarm_sensors)))
            for sender in alarm_sensor.alarm_sensors:
                sender.trig(values["_hostname"], message)
            alarming = True

    if errors > 0:
        problematic = True
        sample_errors += errors
    else:
        problematic = False
    values[SAMPLING_ERR_CH] = sample_errors
        
    update_leds() #this may be redundant


#Send all values not beginning with _ to a graphite server
#and also any error counters >0
def send_data(values: Dict):
    print("% Sending data via ", transmitters)
    total_bytes = 0
    sendlist = {}
    
    for k, v in values.items():
        if not k.startswith("_"): # _xxx are internal only
            sendlist[k] = v

    #Now add any nonzero error counters
    for sensor in sensors:
        (r, c) = sensor.get_errors()
        if r > 0:
            sendlist["errors.read.{}".format(sensor.name)] = r

    for t in transmitters:
        total_bytes += t.send(sendlist)
    
    print(" Total of {} bytes sent".format(total_bytes))


################################## LED test ################################
def led_test_callback(pin: int):
    global leds
    for k in leds: #all off
        GPIO.output(k, 0)
    time.sleep(0.4)
    for k in leds: #all on, in sequence
        GPIO.output(k, 1)
        time.sleep(0.4)
    for k in leds: #all off
        GPIO.output(k, 0)
        time.sleep(0.4)
    #back to normal:
    update_leds()

    
################################## Config file section  ################################
#return true if allowed
def gpio_setup_out(pin: int, value: int=0) -> bool:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, value)
    return True

#return true if allowed
def gpio_setup_in(pin: int, pull=None) -> bool:
    global pins_in_use
    if pin in pins_in_use:
        print("Ignoring - pin {} in use.".format(pin))
        return False
    pins_in_use.add(pin)
    if pull is None:
        GPIO.setup(pin, GPIO.IN)
    else:
        GPIO.setup(pin, GPIO.IN, pull_up_down=pull)
    return True


#Pin is assumed to be configured as input already
def gpio_ledtest_setup(pin: int):
    GPIO.add_event_detect(pin, GPIO.FALLING, callback=led_test_callback, bouncetime=1000)

    
def add_config_class(c):
    global sensor_types
    names = c.confignames() #string not tuple?
    #print(names)
    for name in names:
        sensor_types[name] = c


def read_config(filename: str) -> bool:
    '''# read the config file
    # Format of most lines:
    #     type port channel-name+
    # return true if no fatal errors
    '''
    
    global derivations, leds, sensor_types
    global power_channel
    
    print("% Loading config classes")
    #Main groups of sensors:
    #GPIO
    add_config_class(gpio_bit_sensor.gpio_bit_sensor)
    add_config_class(monitored_bit_sensor.monitored_bit_sensor)
    add_config_class(gpio_counter_sensor.gpio_counter_sensor)
    add_config_class(dust_pigpio_sensor.dust_sensor)
    add_config_class(dht22_sensor.dht22_sensor)
    add_config_class(power_sensor.power_sensor)
    #I2C
    add_config_class(scd30_pigpio_sensor.scd30_sensor)
    add_config_class(tsl2561_sensor.tsl2561_sensor)
    add_config_class(bmp280_sensor.bmp280_sensor)
    add_config_class(bme280_sensor.bme280_sensor)
    add_config_class(si7021_sensor.si7021_sensor)
    add_config_class(am2320_sensor.am2320_sensor)
    add_config_class(ads1015_sensor.ads1015_sensor)
    #Serial and OneWire (both have device ports in the file system)
    add_config_class(ow_temp_sensor.ow_temp)
    add_config_class(ow_counter_sensor.ow_counter)
    add_config_class(mhz14a_sensor.mhz14a_sensor)
    add_config_class(modbus_sensor.modbus_sensor)
    add_config_class(sms_sensor.sms_sensor)
    add_config_class(pms5003_sensor.pms5003_sensor)
    #Internet, fs, etc
    add_config_class(heartbeat_sensor.heartbeat_sensor)
    add_config_class(file_sensor.file_sensor)
    add_config_class(cycles_sensor.cycles_sensor)
    add_config_class(dirsize_sensor.dirsize_sensor)
    add_config_class(celestial_sensor.celestial_sensor)
    add_config_class(concinnity_klimat_sensor.concinnity_klimat_sensor) #With LEDs
    add_config_class(concinnity_ph_sensor.concinnity_ph_sensor)
    add_config_class(concinnity_ping_sensor.concinnity_ping_sensor)
    add_config_class(concinnity_ec_sensor.concinnity_ec_sensor)
    add_config_class(pagerduty_sensor.pagerduty_sensor)
    add_config_class(silent_alarm_sensor.silent_alarm_sensor)
    #transmitters
    add_config_class(graphite_transmitter.graphite_transmitter)
    
    print("  Loaded {} items".format(len(sensor_types)))
    
    print("% Reading configuration file")

    f = open(filename, "r+")
    lines = f.readlines()
    f.close()
    config_list = []
    power_channel = None
    
    for configstr in lines:
        #print(" Config line: " + configstr) #debug
        #split into list on whitespace, discard comments
        config = shlexsplit(configstr, comments=True)
        if len(config) == 0: #empty
            continue
        t = config[0]
        #print(" Config: {}".format(config)) #debug

        try:
            if t in sensor_types:
                sclass = sensor_types[t]
                sinstance = sclass(config) #can fail
                print('  Sensor {}: {}'.format(sinstance.get_name(), sinstance.get_type()))
                continue
        except Exception as E:
            print("Failed to configure {}: {}".format(config, str(E)))
            continue

        ##now check for built-in types
        p = config[1]
        if len(config) >= 3:
            n = config[2].strip()
        else:
            n = None
        if len(config) >= 4:
            n2 = config[3].strip()
        else:
            n2 = None
        if len(config) >= 5:
            n3 = config[4].strip()
        else:
            n3 = None
        if len(config) >= 6:
            n4 = config[5].strip()
        else:
            n4 = None
        if len(config) >= 7:
            n5 = config[6].strip()
        else:
            n5 = None

        #set of built-in types:
        if not t in {'alarm', 
                     'led', 'led_test',
                     'linear', 'threshold', 'compensate', 'lowpass'}:
            print("Ignoring unknown config type: " + t)
            continue
        
        if t == "led":
            p = int(p)
            if not gpio_setup_out(p, 0): #Off by default
                continue
            leds.append(p)
            for k in range(2, len(config)):
                n = int(config[k])
                if not gpio_setup_out(n, 0): #Off by default
                    continue
                leds.append(n)
            print("  LEDs: ", leds)

        if t == "led_test":
            p = int(p)
            if not gpio_setup_in(p, GPIO.PUD_UP): #always pull-up
                continue
            gpio_ledtest_setup(p)

        if t == "alarm":
            a = condition.condition(config) #collects itself
            
        if t == "linear": #linear srcchannel factor constant targetchannel 
            derivations.append((t, p, float(n), float(n2), n3))

        if t == "lowpass": #lowpass srcchannel factor targetchannel 
            derivations.append((t, p, float(n),  n2))

        if t == "threshold": #threshold srcchanel limit bvalue avalue targetchannel
            derivations.append((t, p, float(n), float(n2), float(n3), n4))

        if t == "compensate":  #compensate src-channel factor constant comp-channel
            derivations.append((t, p, float(n), float(n2), n3))

    #for now, remember the power-ok channel
    if len(power_sensor.power_channels) > 0:
        power_channel = power_sensor.power_channels[0]
            
    print("  Created {} sensors, {} alarm conditions, {} derivations, {} data transmitters, {} alarm transmitters".format(
        len(generic_sensor.sensors),
        len(condition.conditions),
        len(derivations),
        len(transmitters),
        len(alarm_sensor.alarm_sensors)))
    
    #indicate if any config is set up
    return len(generic_sensor.sensors) > 0
    
            
def cleanup(signum, frame):
    print("\n%% Cleaning up")

    if len(leds) > 0:
        GPIO.output(leds[0], 0) #'Power' off
    GPIO.cleanup()

    #tell all sensors to close devices/stop threads etc.
    for sensor in generic_sensor.sensors:
        sensor.close()

    exit()


############################### Main program ##################################

print("%%% Concinnity Aquasensor %%%")
print("%% Setup")
GPIO.setmode(GPIO.BCM) #GPIO will use Broadcom pin numbers

pins_in_use = {2, 3, 5, 14, 15}
set_used_ports(['2', '3', '5', '14', '15']) #I2C, Onewire and serial, actually configured in /boot/config.txt
#TODO: pin 4 is hardcoded OneWire in Buster release

derivations = []
leds = []
sampling = False
alarming = False #TODO move to values
problematic = False #TODO split into big and small (or sensors and network?) and move to values
sensor_types = {} #sensor classes indexed by config name(s)
sensors = [] #list of sensor objects

i2c = busio.I2C(board.SCL, board.SDA) #We practically always use the I2C bus - not deferred until config file
    
# RTC is on i2c bus but is handled by operating system

#now we know which pins are in (internal) use, so we can read the dynamic config file
if not read_config("sensornode.conf"):
    print("Configuration has no valid items!")
    exit()

print(" Ports in use: {}".format(str(get_used_ports())))

errors = 0
sample_errors = 0
derivation_errors = 0
cycle_skips = 0
values = {power_channel: 1} #simulate power OK for LED test aftermath
last_values = {}

signal.signal(signal.SIGINT, cleanup)
signal.signal(signal.SIGTERM, cleanup)

led_test_callback(0)

#for debug: run just once, immediately
once = len(sys.argv) > 1 and sys.argv[1].lower() == 'once'
#print(sys.argv, once)
############################### Main loop ##################################

while True:
    #sleep until next whole minute
    if not once:
        time.sleep(60 - datetime.datetime.now().second)

    #remember time
    t1 = time.time()

    print("")
    print("%% Sample cycle " + datetime.datetime.now().isoformat())
    last_values = values
    #clear current values
    values = {}
    #set internal values
    if cycle_skips > 0:
        values[OVERRUN_ERR_CH] = cycle_skips
    if derivation_errors > 0:
        values[DERIVATION_ERR_CH] = derivation_errors
    values["_hostname"] = socket.gethostname() #used by alarms
    values[power_channel] = last_values[power_channel] #For LED until we sample

    sampling = True
    update_leds()

    errors = 0
    
    sample(values) #updates values and errors

    derive(values, last_values) #updates values and derivation_errors

    check_for_alarm_conditions(values, last_values)

    send_data(values)

    sampling = False
    update_leds()

    t2 = time.time()
    dt = t2 - t1
    #print("% Cycle elapsed time = {}".format(dt))
    if dt > CYCLE_SECONDS:
        print("Overrun! Cycle took {}".format(dt))
        cycle_skips += 1

    if once:
        cleanup(signal.SIGTERM, None)
