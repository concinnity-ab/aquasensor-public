import board
import busio
from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import adafruit_si7021

class si7021_sensor(generic_sensor):
    '''Class for a Silicon Labs SI7021 T+RH sensor; I2C-connected.
     ± 3% relative humidity measurements with a range of 0–80% RH, and ±0.4 °C temperature accuracy at a range of -10 to +85 °C
    This sensor has a serial number! Call sensor.readSerialNumber() to read out the 8 bytes of unique ID. Then you can access them from sensor.sernum_a and sensor.sernum_b
    '''

    @classmethod
    def confignames(cls) -> List:
        return ["si7021"]

    
    def __init__(self, config:List):# "si7021" i2caddr tempchannel humchannel
        super().__init__(config)
        #Adafruit default address=0x40
        i2caddr = int(config[1], 16) #Always given in hex.
        if i2caddr > 127 or i2caddr < 0:
            raise ValueError('Invalid I2C address')
        if i2caddr != 0x40:
            raise ValueError('Wrong I2C address - must be 0x40')
        self.tchannel = config[2]
        self.hchannel = config[3]
        self.sensor = adafruit_si7021.SI7021(busio.I2C(board.SCL, board.SDA), address=i2caddr)
        #C library only:
        #self.sensor.readSerialNumber()
        #print(" SI7021 serial number {} {}".format(self.sensor.sernum_a, self.sensor.sernum_b))
        super().register()

        
    def read(self) -> Dict:
        result = {}
        # read temp+humidity
        try:
            result[self.tchannel] = self.sensor.temperature
            result[self.hchannel] = self.sensor.relative_humidity
            #print("SI7021 T {}  H {}".format(self.sensor.temperature, self.sensor.relative_humidity))
            return result
        except Exception as e:
            self.read_errors += 1
            raise Exception("SI7021 read failed: " + str(e))

    

    

