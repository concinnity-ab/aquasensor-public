from typing import List
from typing import Dict
from alarm_sensor import alarm_sensor

class silent_alarm_sensor(alarm_sensor):
    '''A "sensor" that can send alarms to the console. 
    Purely for testing purposes.'''


    @classmethod
    def confignames(cls)->List:
        return ["silent_alarm"]

    
    def __init__(self, config:List):
        ''' silent_alarm filename channelname'''
        super().__init__(config)

        self.alarm_filename = config[1]
        self.channel = config[2]
        super().register()

        
    def read(self) -> Dict:
        print("Silent alarm status checked")
        result = {}
        try:
            result[self.channel] = 1.0
        except Exception as e:
            errors = errors + 1
            print(" Silent read error: ", str(e))
        return result

    
    def close(self):
        print("Silent alarm closed")


    def trig(self, hostname: str, message: str):
        super().trig(hostname, message)
        print("Silent alarm triggered. Count = {}".format(self.send_count))
        print(message)

    
    def clear(self, hostname:str, message:str):
        super().clear(hostname, message)
        print("Silent alarm cleared")
        print(message)
    
