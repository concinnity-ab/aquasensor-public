from typing import List
from typing import Dict
from alarm_sensor import alarm_sensor
import gammu

class sms_sensor(alarm_sensor):
    '''A "sensor" that can send SMS alarms via 3G modem
    using Gammu library (which has to be installed).'''

    sm = None #Gammu state machine

    @classmethod
    def confignames(cls)->List:
        return ["sms"]

    
    def __init__(self, config:List):
        ''' sms - alarmlist-filename signal-strength-channel
        TODO have a modem filename in case of several USB devices? (why?!)''' 
        super().__init__(config)

        self.alarm_filename = config[2]
        self.channel = config[3]

        #let Gammu find the modem.
        #If it can be confused w other USB devices, we will need a devfile parameter
        self.sm = gammu.StateMachine()
        self.sm.ReadConfig()
        self.sm.Init() #connects to phone
        netinfo = self.sm.GetNetworkInfo()
        print(netinfo)
        super().register()

        
    def read(self) -> Dict:
        #check SMS modem status
        result = {}
        try:
            netinfo = self.sm.GetSignalQuality()
            print(self.get_name(), " signal quality: ", netinfo)
            result[self.channel] = netinfo['SignalPercent']
        except Exception as e:
            errors = errors + 1
            print(" SMS modem read error: ", str(e))
        return result

    
    def close(self):
        self.sm.Terminate() #disconnect from phone
        pass


    def trig(self, hostname: str, message: str):
        '''Sends the message to the numbers in the file.
        We reread the file on every trig, in case it changed.'''
        super().trig(hostname, message)
        
        try:
            # read alarm list file
            f = open(alarm_filename, "r+")
            lines = f.readlines() # lines items includes terminating newline(!)
            f.close()

            #first line was the message subject template
            message2 = lines[0].strip().format(hostname) + message

            #remaining lines are phone numbers, possibly commented out/followed by a #comment
            for i in range(1, len(lines)-1):
                phone = lines[i].split("#")[0].strip() #strip to remove any newlines
                if phone is not None and phone is not "":
                    print("Sending alarm to " + phone + ": " + message)
                    try: #try every number even if one fails
                        message = {
                            'Text': message,
                            'SMSC': {'Location': 1},
                            'Number': phone
                        }

                        self.sm.SendSMS(message)
                        time.sleep(5.0) #let it be sent(???)
                    except Exception as e:
                        print(" Sending SMS to {} failed: {}".format(phone, str(e)))
                        self.send_errors += 1
        except Exception as e:
            print(" SMS Sending failed: {}".format(str(e)))
            self.send_errors += 1


    
    def clear(message:str):
        super().clear(hostname, message)
        #We could send a 'normal again' message here
        pass
    
