from typing import List, Dict

transmitters = []

nextnumber = 1


class transmitter:
    '''Base class of all data transmitters'''

    name = ""
    
    @classmethod
    def confignames(cls)->List:
        return [] #makes it abstract


    def __init__(self, config:List):
        '''Remember basic config; give it a name'''

        global transmitters, nextnumber

        self.transmitter_type = config[0]
        self.name = "T{}".format(nextnumber)
        nextnumber += 1

        transmitters.append(self)
    
        
    def send(self, values: Dict, lastValues: Dict) -> int:
        return 0 #no bytes sent
        
    def get_name(self):
        return self.name

    
    def get_type(self):
        return self.transmitter_type
    

    def get_errors(self):
        return 0
    
