import board
import busio
from typing import List
from typing import Dict
from generic_sensor import generic_sensor
import adafruit_tsl2561

class tsl2561_sensor(generic_sensor):
    '''Class for a TSL2561 illumination (Lux) sensor; I2C-connected'''

    @classmethod
    def confignames(cls) -> List:
        return ["tsl2561"]

    
    def __init__(self, config:List):# "tsl2561" i2caddr luxchannel
        super().__init__(config)
        i2caddr = int(config[1], 16) #Always given in hex.
        if i2caddr > 127 or i2caddr < 0:
            raise ValueError('Invalid I2C address')
        self.channel  = config[2]
        self.sensor = adafruit_tsl2561.TSL2561(busio.I2C(board.SCL, board.SDA))
        super().register()

        
    def read(self) -> Dict:
        # read light level
        try:
            lux = self.sensor.lux
            print(" Lux:", lux)
            if lux is None: #underrange OR overrange
                lux = 0.0 #underrange is most likely (indoors)
            return {self.channel:lux}
        except Exception as e:
            self.read_errors += 1
            raise Exception("TSL2561 read failed: " + str(e))

    

    

