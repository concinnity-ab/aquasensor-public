#!/usr/bin/env python3

import socket
import time
import pagerduty_events_api

'''Concinnity Watchdog - A simple socket watchdog to trigger a pagerDuty alarm if a client goes silent'''

HOST = ''  # Becomes INADDR_ANY
PORT = 2222 # Port to listen on (non-privileged ports are > 1023)
PD_KEY = "40fb7f0d8e5449428c4dce5124ac0dea"

TIMEOUT_S = 150 #has to miss two packets before we react
CHECK_INTERVAL_S = 15

memory = {} #key is message, value is time
pd_service = None
pd_incident = None

def send_pagerduty_alarm(hostname: str, message: str):
    global pd_service, pd_incident
    if pd_service is None:
        return

    if pd_incident is not None: #PagerDuty knows
        return

    print(" Sending Pagerduty incident trigger: '" + message + "'")
    try:
        pd_incident = pd_service.trigger(hostname + ': ' + message)
        print("  incident: ", pd_incident)
    except:
        print("Pagerduty trigger failed!")
        pd_incident = None

        
def resolve_pagerduty_alarm(hostname: str, message: str):
    global pd_service, pd_incident
    if pd_service is None:
        return

    if pd_incident is None:
        print(" No Pagerduty incident to resolve: " + message)
        return

    print(" Resolving Pagerduty incident: " + message)
    try:
        pd_incident.resolve({'description': message})
    except:
        print("Pagerduty resolution failed!")
    pd_incident = None

    
def pagerduty_setup(service_key: str):
    global pd_service
    #try:
    pd_service = pagerduty_events_api.PagerdutyService(service_key)
    #except Exception as e:
    #    print("Pagerduty setup failed")
    #    pd_service = None


with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
    s.bind((HOST, PORT))
    s.settimeout(5)
    print("Concinnity Watchdog listening on UDP port {}".format(PORT))

    pagerduty_setup(PD_KEY)
    lastcheck = time.time()
    
    while True:
        try:
            data = s.recvfrom(2048)
            message = data[0].decode("utf8")
            sender = data[1]
            when = time.time()
            memory[message] = when
            print(' Heartbeat from {}: "{}"'.format(sender[0], message))  #debug
        except socket.timeout:
            print (" Timed out receiving") #debug

        if time.time() - lastcheck > CHECK_INTERVAL_S:
            print(" Checking if any of the {} hearts have stopped beating".format(len(memory))) #debug
            lastcheck = time.time()
            any = False
            for server,when in memory.items():
                age = time.time() - when
                if age > TIMEOUT_S: 
                    any = True
                    if pd_incident is None:
                        send_pagerduty_alarm("Watchdog misses ", server)
                    continue #only mourn for one

            if not any and pd_incident is not None:
                resolve_pagerduty_alarm("Watchdog", "Everybody lives")
