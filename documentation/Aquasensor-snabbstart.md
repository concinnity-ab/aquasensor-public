Concinnity Aquasensor
=====================

eller hur man gör en mätdator för några tusen.

Kunskapskrav
------------
Du behöver kunna sätta i kontakter (åt rätt håll!) och koppla ihop små saker med sladdar. För lådan behöver du borra hål, skruva skruvar och gärna täta efteråt med silikon. Kunna löda är inte nödvändigt, men kan ge ett prydligare och mer robust resultat. För att installera och konfigurera mjukvaran behöver du kunna ge enklare Linux-kommandon från kommandoraden.

Hårdvara för att komma igång
----------------------------
1. Raspberry Pi (RPi) 3 Model B+ (eller RPi 4 Model B, men den är dyrare, drar mer ström och blir varmare). Storlek som ett kreditkort. Hos Kjell m. fl. 400-500kr.
2. USB-laddare som ger minst 2,4A. 200kr
3. Mikro-SD-kort. Minst 16GB. 100-200kr
4. Tangentbord, mus och bildskärm/TV med HDMI-kabel. (Behövs bara i början.)
5. Internetanslutning.

Hårdvara för drift
------------------
6. Nån sorts låda plus distanser och skruvar för att skuva fast datorn i lådan. Allt från en tät matlåda till en IP68-klassad elcentral. Ta till ordentligt. Blir pilligt om lådan är för liten.
7. Realtidsklocka. En liten modul man sätter på stiftlisten på sin RPi. PiRTC eller RasClock från m.nu eller DS3231 fr Electrokit.
8. Backup-batteri. Powerpack med passthrough (dvs kan laddas medan den levererar ström). 5000 mAh eller mer.

1,2,3&6 kan också säljas tillsammans i nybörjar-satser (men den lådan är för liten).

Sensorer
--------
Adderas efter behov. Just nu stöds: Temperatur i luft & vatten, luftfuktighet, koldioxidhalt, ljusstyrka, el- och vattenmätare, allmänna larmgivare som nivåbrytare etc, pH och syrehhalt i vatten. Sen behöver man någon typ av koppling mellan sensorer och dator:
* Delbara kopplingskablar i glada färger. Finns hos alla som säljer sensorer. 
* Pimoroni Breakout Garden, jättenkel att använda, men begränsat utbud av sensorer och för de som ska sitta en bit bort, t.ex. utanpå lådan, måste man löda.
* Experimentkort med skruvplintar. Kräver ingen lödning och sitter bra. T.ex. Pi-EzConnect från Electrokit.

Mjukvara
--------
Du behöver grundläggande kunskaper i Linux: hur man loggar in till en kommandotolk, vad sudo gör, hur man installerar program med apt-get, hur man editerar en textfil, etc. Du behöver också en annan dator som kan ladda ned ett operativsystem och skriva det till SD-kortet. För att se resultaten behövs också en webbserver med Grafana och Graphite installerat.

Leverantörer och länkar
-----------------------
* Kjell och Company, www.kjell.com, med butiker överallt och postorder.
* Electrokit, www.electrokit.com, postorder.
* m.nu, www.m.nu, postorder.
* Pimoroni, www.pimoroni.com, postorder (fr Storbritannien).
* www.raspberrypi.org, hjälp med och nedladdning av operativsystem.

Hur man kommer igång
====================

Installation av hårdvara
------------------------
Koppla in tangentbord, mus och skärm (eller TV). Tryck fast realtidklockan på stiftlisten, längst bort från USB-kontaktens ände.

Installation av mjukvara
------------------------
I stora drag:
1. Du behöver Raspberry Pi OS med desktop (Debian-baserad Linux för RPi). Finns på https://www.raspberrypi.org/downloads/raspberry-pi-os/ Senaste versionen heter just nu Buster. Läs installationsguiden på raspberrypi.org, ladda ner .img-filen till en annan dator och skriv den till SD-kortet.
2. Sätt SD-kortet i din RPi (på undersidan), koppla in strömkabeln. Datorn ska nu boota.
3. Följ installationsguiden så du får tangentbord, språk, och nätverk att fungera. Låt operativsystemet uppdatera sig. (Kan ta en stund.)
4. Bekanta dig med din RPi. *Byt lösenordet*.
5. Kör konfigureringsprogrammet från startmenyn och slå på I2C och OneWire (om du har såna sensorer). Slå på ssh om du vill kunna logga in över nätet.
6. Redigera (t.ex med `sudo nano`) /boot/config.txt för att beskriva din realtidsklocka. Boota om med `sudo reboot`.
7. Ladda ner aquasensor-paketet från GitLab: `git clone git@gitlab.com:concinnity-ab/aquasensor-public.git aquasensor`
8. Gå till setup-katalogen: `cd aquasensor/setup/`
9. Installera grafsy med: `sudo ./grafsy_setup.sh`
10. Redigera bin/sensornode.conf för att matcha de sensorer du har.
11. Redigera /etc/grafsy.conf för att peka på din Graphite-server.
12. Starta grafsy med `sudo systemctl start grafsy`.
13. Starta sensornode med `sudo systemctl start sensornode`.

Exempel på sensorer
--------
* DS18B20, temperatursensor (vattentät). Kjell 87081. OneWire.
* AM2320, temp och luftfuktighet. Kjell 90647. I2C.
* COUNTV2, räknarmodul för el- och vattenmätare (med S0-utgång). m.nu. OneWire. Backup-batteri. *Koppla inte in 5V från RPi*.
* Si7021, temp och luftfuktighet. I2C. Electrokit.
* BME280, temp, luftfuktighet och lufttryck. m.nu eller Pimoroni. I2C.
* BMP280, temp och lufttryck. m.nu eller Electrokit. I2C.
* MH-Z14B, koldioxidhalt. Ebay. Serieport. *5V*
* SCD30, kodioxidhalt, temperatur och luftfuktighet. I2C.
* ADS1015, A/D-omvandlare (spänningsmätare). Pimoroni eller m.nu. I2C. Till den kan man sen koppla sensorer som syreprober, pH-prober osv.
* TSL2651, ljusstyrka i Lux. I2C.

Ett schema för hur man kopplar in dem finns på https://pinout.xyz/ Alla sensorer kopplas till jord (GND) och drivspänning (VCC/VDD/VIN) till 3,3V (ibland 5V). I2C-sensorer kopplas till SCL och SDA. För serieportsensorer kopplas RX till TX och TX till RX. OneWire-sensorer kopplas till pinne 4, och ett motststånd på 4,7 kΩ kopplas från pinne 4 till 3,3V.

Kopplingskablar finns som hane-hane, hona-hona och hane-hona. Många sensorer har stift och RPi har det också, så man klarar sig ofta med hona-hona. (Han-ändar kan också skruvas i plintar.) T.ex. Kjell 87075.

Indikatorer
-----------
Om man vill se att datorn lever och arbetar kan man koppla in en eller flera lysdioder mellan lediga pinnar på sin RPi och GND, i serie med ett motstånd på minst 680 Ω. 
