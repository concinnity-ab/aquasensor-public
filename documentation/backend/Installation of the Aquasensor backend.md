## Setup Blue-Green Aquasensor backend on an Intel NUC

This document describes how to set up the software for the sensors backend on a [NUC](https://www.intel.com/content/www/us/en/products/boards-kits/nuc.html). This is of course only an example of how the backend can be created and run, but the hope is that it can serve as a guide for others while providing a good real world solution.

### Licenses
Blue-Green Aquasensor code for sensor nodes and backend configuration is available under [AGPL3](https://www.gnu.org/licenses/agpl-3.0.en.html). Other software (Docker etc.) is available under their respective licenses.

This documentation is licensed under [Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). 

Contact us at info@concinnity.se if you are interested in any other licensing arrangements for our software.

### Hardware
The [Intel NUCs](https://en.wikipedia.org/wiki/Next_Unit_of_Computing) are tiny "bare-bone" computers. They come in little boxes and usually the user selects and adds memory and storage to the unit. In our case we use a NUC7CJYH, a fairly recent version with a Celeron based two-core processor. It should be more than adequate for our needs.

We install 8GB of memory, there is a list of memory that works with the unit [here](https://compatibleproducts.intel.com/ProductDetails?EPMID=126135). In our case we use 2x4GB from Corsair (CMSX8GX4M2A2400C16, a smaller size of one listed as validated by Intel).

We install a 240GB SSD drive, a Kingston A400 240GB 2.5", but any SATA SSD or hard drive in the 2.5" form factor should do. Since the database may be fairly large, depending on how many sensors that collect data and the chosen retention scheme, we suggest to have at least 50 GB of storage.

### Operating system image creation
For operating system we have chosen Debian. It is a very solid Linux distribution and we feel that the stability provided by the Stable branch of Debian far outwheighs that some packages may not be the most recent version.

To install Linux we create a bootable USB stick using the [64-bit PC netinst iso](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.4.0-amd64-netinst.iso). This image is fairly minimal and a good internet connection is recommended for the additional downloading of packages. On the [Debian distributions downloads page](https://www.debian.org/distrib/) there are larger images with more packages as well as images for other processor architectures.

Using the .iso file we create the bootable USB stick. The stick needs to be of 1GB size or larger. Make sure there is no data on the USB stick that you need since the whole stick will be overwritten and only contain the files needed for the booting and installation of Debian.

On Linux use [these instructions](https://linuxize.com/post/create-bootable-debian-10-usb-stick-on-linux/) to create the bootable USB stick.

On Windows we recommend the use of [Rufus](https://rufus.ie/). [This is a good guide](https://www.techrepublic.com/article/pro-tip-use-rufus-to-create-a-bootable-usb-drive-to-install-almost-any-os/) to using Rufus.

On Mac OS [this page](https://www.lewan.com/blog/2012/02/10/making-a-bootable-usb-stick-on-an-apple-mac-os-x-from-an-iso) has a good description on how to make a bootable USB image. You can also use [Etcher](https://www.balena.io/etcher/) as described on [this Ubuntu page](https://ubuntu.com/tutorials/create-a-usb-stick-on-macos#1-overview).

The NUC may need a little preparation to allow the installation from the USB memory. On the NUC7CJYH2 the boot order needed to be changed so USB had higher priority than the SSD drive. This is changed in the BIOS that can be reached by pressing the F2 key right after the NUC is started. [This is a short guide](https://sites.google.com/site/visualbios/tutorials/boot-order-tutorial) on how to change the boot order.

### Install Debian
We should now be ready to install Debian. It is recommended that the machine is connected to the internet while doing the installation as part of the process is to set up a package manager mirror from which additional software is downloaded that is not part of the USB stick image. Without internet access you will not be able to add a graphical user interface as part of the installation, but will be left with a terminal only "server setup". It is of coure possible to add a GUI later, however this is outside of what we describe in this document.

Restart the computer with the USB memory inserted and it will start from the stick and display the Debian installation page. You should now be presented with the Debian installation page:

![Debian installation page](img/inst-boot.png)

_Debian installation page_

We select Graphical installation. This provides a nice UI for the installation process.

The first installation steps let you select language, location, locale and keyboard.

![](img/00_localechooser_languagelist_0.png)

_Select language both for the installation and the system_

---
Now we need to place ourselves on the map.
![](img/01_localechooser_shortlist_0.png)

_Find your way to where you are (1)_

---
![](img/02_localechooser_continentlist_0.png)

_Find your way to where you are  (2)_

---
![](img/03_localechooser_countrylist_Europe_0.png)

_Find your way to where you are (3)_

---
Select the locale. Since we chose English we choose the US locale.
![](img/04_localechooser_preferred-locale_0.png)

_Select locale_

---
We select the Swedish keyboard. This works well even if the locale is en_US. Your choice may differ depending on preferences.
![](img/05_keyboard-configuration_xkb-keymap_0.png)

_Select keyboard map_

---
Now we are prompted for hardware drivers. We select No here, this will be addressed later in the installation process.
![](img/06_hw-detect_load_firmware_0.png)

_Hardware drivers page_

---
The system needs a hostname. We choose `sensordata` but this is of course arbitrary.
![](img/07_netcfg_get_hostname_0.png)

_Hostname selection_

---
We will not use a domain name.
![](img/08_netcfg_get_domain_0.png)

_No domain name_

---
Add a root password. It should be long and hard to guess. Write it down!
![](img/09_passwd_root-password_0.png)

_Set the root password_

---
Create a user account, starting with adding the user's real name. We set it to _Sensor Data_.
![](img/10_passwd_user-fullname_0.png)

_Real name of user_

---
Set the account's username.
![](img/11_passwd_username_0.png)

_Account username_

---
Set the account password
![](img/12_passwd_user-password_0.png)

_Account password_

---
Now it's time to select where the software will be installed. We choose most straightforward scheme, using the entire disk.
![](img/13_partman-auto_init_automatically_partition_0.png)

_Disk partitioning_

---
Since the USB stick counts as a disk we are prompted to select the disk we will install to.
![](img/14_partman-auto_select_disk_0.png)

_Select the SSD drive_

---
We choose the simplest partitioning scheme with all files in one partition.
![](img/15_partman-auto_choose_recipe_0.png)

_Simplest partitioning scheme_

---
We are now presented with a view listing the currently selected partitioning scheme. If all looks correct we continue. Otherwise you can go back and change any errors.
![](img/16_partman_choose_partition_0.png)

_Verify the disk partitioning_

---
Confirm the writing of data to the disk.
![](img/17_partman_confirm_0.png)

_Last chance!_

---
Now the software will be installed to the disk. This will take some time. At the end of the writing of files to the disk we need select a Debian archive mirror. This is a server on the internet that serves copies of all packages provided in Debian. We start with selecting Sweden since that is where we are based.

![](img/18_mirror_http_countries_0.png)

_Select a country for the Debian archive mirror_

---
We are shown a list of actual mirrors. The recommended choice is `deb.debian.org` whcih is prefectly fine of course but we choose a mirror in Sweden, `debian.mirror.su.se` since physical proximity has a small impact on download speed.
![](img/19_mirror_http_mirror_0.png)

_Select an archive mirror_

---
Next up we are prompted for any proxy that we need to use for internet access. In our case we have no proxy.
![](img/20_mirror_http_proxy_0.png)

_We don't need to set up an HTTP proxy_


---
The installer will now download and install any updates of the operating system software.

At the end of the updating we are prompted for participation in the "popularity contest". This is a system where information about the packages installed is sent anonymously to the Debian organisation to help them determine what software to include in future installation images like the one we use here. We choose to be included in the "contest" but this is of course entirely optional.
![](img/21_popularity-contest_participate_0.png)

_Popularity contest participation_

---
We can now select additional software to install as part of the setup. We choose the Debian desktop environment, a simple and clean GUI, SSH server and the standard system utilities. It is very handy to be able to use SSH to access the machine from another computer. if you plan on running the machine completely in "headless" mode you will not need a GUI and can skip that from the selection. However, we recommend using a desktop environment, as then you can have these instructions in a browser and just cut and paste the terminal commands into the terminal. This will make the installation a lot easier. Otherwise there are instructions below on how to switch between using the GUI and just run the command line.

![](img/22_tasksel_first_0.png)

_Select additional software_

---
All additional software is now installed. This may be a somewhat lengthy process depending on the choices made.

When all software has been installed we are done. We can now remove the USB stick and restart the computer.

![](img/23_finish-install_reboot_in_progress_0.png)

_Finished!_


## System setup

### Get a terminal to write commands in

Login to the system with your username and password. If you selected a GUI environment, then you will see a desktop. Click the menu Activities in the top left corner. Then in the field that says "Type to search..."  type in Terminal and select the terminal. Now you can also drag and drop the terminal icon to the toolbar on the left.

Now the system needs to be prepared for using docker and docker-compose.

### Add user to sudoers
The user created as part of the installation does not have full permissions on the system. To enable that we need to add the user to the "sudoers" group. We will use `sensordata` as the account username in the following, you need to replace it with your chosen username where applicable.

Assume root: `su -` This will prompt you for the root password.

Now add `sensordata` to sudoers group: `usermod -a -G sudo sensordata`

Assume sensordata: `su - sensordata`

Check that your user account has the right groups: Type the command `groups` The response will be something like `sensordata cdrom floppy sudo audio dip video plugdev netdev bluetooth scanner`. We are looking to verify that `sudo` is part of this list.


### Install firmware
The NUC needs drivers for its hardware, WiFi, bluetooth and audio. To enable those Intel software needs to be installed. We use the following steps.

Add sources for non-free software: `sudo nano /etc/apt/sources.list`

This opens the soures.list file in the simple text editor nano ([Quick introduction here](https://www.linode.com/docs/tools-reference/tools/use-nano-text-editor-commands/)). In the sources.list file add:
```
deb http://deb.debian.org/debian buster main contrib non-free
deb-src http://deb.debian.org/debian buster main contrib non-free

deb http://deb.debian.org/debian-security/ buster/updates main contrib non-free
deb-src http://deb.debian.org/debian-security/ buster/updates main contrib non-free

deb http://deb.debian.org/debian buster-updates main contrib non-free
deb-src http://deb.debian.org/debian buster-updates main contrib non-free
```

Save the file.

Update the apt index: `sudo apt-get update`

Install firmware for the NUC hardware: `sudo apt-get install -y firmware-iwlwifi`

### Install docker and docker-compose

Make sure no old version of docker is present: `sudo apt-get remove docker docker-engine docker.io containerd runc` On a freshly installed system this will report that several of those packages weren't installed.

Prepare apt to use a repository over HTTPS: 
```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

Add Docker's public GPG key: `curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -`	

Verify that you have the correct key: `sudo apt-key fingerprint 0EBFCD88`
This should return: 
```
pub   rsa4096 2017-02-22 [SCEA]
      9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
sub   rsa4096 2017-02-22 [S]
```

Now add the **stable** repository: 
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable"
```

Update the apt index once more: `sudo apt-get update`

Now we can install Docker: `sudo apt-get install -y docker-ce docker-ce-cli containerd.io`

This will take a little while.

Finally test that docker installed correctly: `sudo docker run hello-world`

This will download and run a test image that displays a message and then exits.

A nice convenience is to be able to run docker-related commands without using `sudo`. This can be accomplished with the following steps.

Create the _docker_ group: `sudo groupadd docker`

Add yourself to the new group: `sudo usermod -aG docker $USER`

Activate the change (one can also log out and in again): `newgrp docker`

Test that you can use docker without sudo: `docker run hello-world`

Note that this addition is a security issue since the docker group is granted privileges equivalent to root. However in the context of running a machine that is not connected to the internet it would be OK in most cases. If you're exposing the machine to the "high seas of the internet" you will need to look into securing it anyway. There is a [security section of the docker documentation](https://docs.docker.com/engine/security/security/) that will be relevant in this case. We DO NOT recommend that you run this machine directly connected to the internet. You should install it at a minimum behind a firewall. 

### Install Docker-compose

We can now create the containers that will run the Blue-Green Aquasensor backend. The documentation for docker-compose writes the following introduction: "Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration."

We see Compose as a very handy tool for small systems of containers. The configuration of all containers resides in one file, where they can refer to each other making networking trivial and you have one place where you find all of the information needed to run the containers.

This is of course only true to a degree. Very soon you might use additional files for such things as environment variables and custom container images. But the simplicity of the core tool and its commands are nonetheless very powerful.

The download and installation of Docker-compose requires the following:

```
sudo apt-get install -y python-dev python-pip libffi-dev \
    libcurl3-openssl-dev gcc libc-dev make
```

followed by:

`sudo -i`

Now we can download and install docker-compose:
```
sudo curl -L \
    "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
```
You might want to check if there is a newer version of Compose than 1.26.2 [here](https://docs.docker.com/compose/install/#install-compose-on-linux-systems).

Now we set the execute permission on docker-compose:

`chmod +x /usr/local/bin/docker-compose`

Then we exit from sudo mode:

`exit`

Check that Compose works correctly: 

`docker-compose --version`


### Create external volumes

Data incontainers are "ephermal" by default. When the container is removed so is its data. This is not a good idea for our purposes. Graphite needs to store the sensor data in a permanent place and the settings and user-created dashboards in Grafana also need persistence. To accomplish this we create external volumes for both containers with the following:

```
docker volume create --name=grafana-data
docker volume create --name=graphite-data
```

At the bottom of the docker-compose.yml file you can see references to those two volumes. In practice this means that the data will reside on the host system. 

You can find the path to the external volumes using:
 
`docker volume inspect grafana-data`

and:

`docker volume inspect graphite-data`

This will be handy when making backups. Note that it is recommended to shut the containers down while backing up.

### Setup docker-compose.yml and associated resources

With docker-compose  and volumes in place we are finally ready to set up the configuration files for the containers and get them running. Start by checking out the repository on the NUC and then copying the **backend** folder to the desired place.

We will use [git](https://git-scm.com/) to copy the files needed to the NUC. This is done by what git calls cloning of the repository: 

`git clone https://gitlab.com/concinnity-ab/aquasensor-public.git`

This will create a new folder, **aquasensor-public**, in the current folder containing the full content of the repository. If you do this in the `tmp` or `Downloads` folder you can delete it after having copied **backend**. We only need the **backend** folder so we copy it to the home folder:

`cp -avr aquasensor-public/backend/ ~/`

Cd into the **backend** folder
 
`cd ~/backend` 

This is now the "root" folder for running the containers. It contains a shell script file, `configure-nginx.sh` a yaml file, `docker-compose.yaml` and a folder called **data**.

Make configure-nginx.sh executable

`chmod +x configure-nginx.sh`

We can now configure nginx. We need the IP address of the machine to be included in the configuration file `app.conf` for it to rout http requests to Grafana correctly. To find the IP address we use 

`ip route`

It will return something like the following:

```
default via 192.168.1.1 dev eno1 proto dhcp metric 100
172.17.0.0/16 dev docker0 prot kernel scope link src 172.17.0.1 linkdown
192.168.1.0/24 dev eno1 protp kernel scope link src 192.168.1.200 metric 100
```

We look for the LAN addresses, in many cases they start with `192.168...` and we can read the name of the interface as the string after `dev`, in the example above that would be `eno1` which is the name of the network interface on the NUC.

Now we run

`./configure-nginx.sh eno1`

(possibly substituting `eno1` with the correct network interface ID) to create the nginx configuration file.

### docker-compose basics

Docker-compose allows you to manage the containers defined in a yaml file. This is done using the command `docker-compose` followed by options and sub-commands. We call this a Compose command.

All Compose commands require a configuration file containing the definitions of the containers. This is called the Compose file. When running a Compose command it will look for a file named `docker-compose.yaml` in the current directory, if it finds one it will be used as the Compose file for the command.

In our case this means that when commands are run in the **backend** folder the docker-compose.yaml file will be automatically included as the Compose file. If you are in another directory, or if the Compose file has been renamed, it needs to be included in the command. E.g. if we are in the home folder and want to run a command it would be like so:

`docker-compose -f backend/docker-compose.yaml up -d`

Short list of basic Compose commands:
 * `docker-compose up` build (if needed) and start all containers
 * `docker-compose up -d` build (if needed) and start all containers in the background
 * `docker-compose up grafana` build (if needed) and start container named _grafana_
 * `docker-compose down` stop and remove containers, networks, volumes and images created by `up`
 * `docker-compose start grafana` start the existing container _grafana_
 * `docker-compose stop grafana` stop the existing container _grafana_
 * `docker-compose logs -f` continuously writes the logs of the running containers to the terminal. This is essentially "included" when you run `docker-compose up`

There are of course a bunch of other commands and options. [Full reference to Compose commands](https://docs.docker.com/compose/reference/).

### Run the containers!

We are finally ready to launch the containers. In the **backend** folder run: 

`docker-compose up`

We can now see that the images of the containers are being downloaded and then executed. When the containers start running the logs from them are output in the terminal. Seeing the logs the first time you run is useful to make sure that everything runs smoothly, but the drawback is that all containers will be stopped and removed when you close the terminal or press Ctrl-C!

Open a new terminal, cd to the **backend** folder and type `docker-compose ps`

You should see something similar to:
```
  Name                Command              State               Ports            
--------------------------------------------------------------------------------
grafana    /run.sh                         Up      3000/tcp                     
graphite   /entrypoint                     Up      0.0.0.0:2003->2003/tcp,      
                                                   2004/tcp, 2013/tcp, 2014/tcp,
                                                   2023/tcp, 2024/tcp, 80/tcp,  
                                                   8080/tcp, 8125/tcp, 8125/udp,
                                                   8126/tcp                     
nginx      /bin/sh -c while :; do sle      Up      0.0.0.0:80->80/tcp  
```

This lists the running containers and shows thier status.

To avoid "locking a teriminal" when we run `docker-compose up` we can run the containers in detached mode. First we bring the containers down: 

`docker-compose down`

As was listed above this stops and removes containers, networks, volumes. We then run 

`docker-compose up -d`

to start the services in detached mode. This returns us to the terminal and the containers will continue to run. In the docker-compose.yaml file there is a line `restart: always` for each container definition. This means that the containers also will be automatically restarted after a computer restart.

It may sound harsh that the `down` command "stops and removes containers, networks, volumes", but is actually the magic of Docker. Everything needed to run a service is contained in the configuration of the service. In our case that is the docker-compose.yaml file and the `up` command uses it to fetch, build and run the services. When a service is brought down we don't need the artifacts that were created to run it and thus they can be removed. However Docker maintains a sophisticated caching scheme that allows the service to be restarted quickly if nothing has changed in the configuration files. This is the reason you only see the downloading of Docker images the first time a service is brought up, or when the version of the image is changed in the Compose file.

The one exception to this is persistent data. This is the reason we created the external volumes. They hold the "user created" data that is not part of the configuration of the services and thus we need to make certain it "survives" the shutting down and removal of the containers.

### Send test data to Graphite

To test the system we want to send data to Graphite and be able to view it in a Grafana graph. We will do this by sending a random number to Graphite once per minute and then we will see the results in Grafana.

Open a new terminal and run the following harrangue:
```
export IP_ADDRESS=$(ip addr show eno1  | awk '$1 == "inet" { print $2 }' | cut -d/ -f1); while true; do echo "test.random $((RANDOM % 500)) `date +%s`" | nc -v -w1 ${IP_ADDRESS} 2003; sleep 60; done
```

Note that you may have to replace `eno1` with the network interface ID of your machine. This command now sends a random number between 0 and 500 once per minute to the label "test.random". If you want to send data more often change `sleep 60` to the number of seconds between messages. A response like `Connection to 192.168.1.200 2003 port [tcp/cfinger] succeeded!` or `sensordata [192.168.1.200] 2003 (cfinger) open` indicates that the data is collected by Graphite.

### Configure Grafana

Next we set up Grafana with a data source and create a dashboard with our random data.

Open a browser and put the IP address of the NUC in the URL field. This will connect to Grafana and you will be shown the login screen. 

![](img/grafana_login.png)

_Grafana login_

---
Log in using **admin** for both username and password. You will immediately be prompted for a new password and then you will be directed to the configuration screen. Here we need to set up Graphite as the data source. 

![](img/change_password.png)

_Set new password_

---
![](img/grafana_setup_home.png)

_Configuration home_

---
 Click on _Add data source_. Select Graphite.
![](img/select_graphite.png)

_Select Graphite_

---
We need to define the URL where Grafana can access the Graphite data. Compose defines an internal network for its containers where the name of the container becomes the host name. We use this to set the URL of Graphite to `http://graphite:8080`
![](img/graphite_url.png)

_Set Graphite URL_

---
Click on the _Save & Test_ button. You should be greeted by a message saying the data source is working. Click on the Grafana logo top left to return to the configuration screen.
![](img/datasource_save.png)

_Save & test_

---
Click on the _New dashboard_ button, select _Add Query_.
![](img/new_dashboard.png)

_Click New dashboard_

---
![](img/add_query.png)

_Click Add Query_

---
Click on the _select metric_ button and choose _test_. Click on the new _select metric_ and choose _random_.
![](img/select_metric.png)

_Select metric_

---
![](img/select_test.png)

_Select test_

---
![](img/select_metric_again.png)

_Select metric once more_

---
![](img/select_random.png)

_Select random_

---
You should now see a graph of random data. Click the diskette symbol to save the dashboard. Give it a descriptive name.
![](img/random_data.png)
_Data!_

---
![](img/save_dashboard.png)

_Save the dashboard_

---
![](img/random_data_dashboard.png)

_Name the dashboard_

---
![](img/final_dashboard.png)

_Final view of the dashboard_

You should now have a dashboard displaying random data. This concludes the setup!

## Additional notes

A document of this kind can of course only brush the surface of everything you can do and everything you need to know to run a system of this kind in any sort of "serious" context. But hopefully we have at least have given some insight into what's needed to set up a simple monitoring service and maybe piqued your interest enough to try it out!

### Boot to command line

When running the system it is not necessary to have the GUI running. It draws extra resoures that won't be needed in "production" mode. 

To disable the gui run `sudo systemctl set-default multi-user.target` and restart the machine.

To enable the GUI again run `sudo systemctl set-default graphical.target`

### Useful resources
The folowing pages/sites provide a lot of the information we have used when creating this document.
 * Intel's [compatibility tool](https://compatibleproducts.intel.com/ProductDetails?activeModule=Intel%C2%AE%20NUC) especially to acertain that you use compatible memory when putting the NUC together from parts.
 * Intel's [support site for the NUC](https://www.intel.com/content/www/us/en/support/products/98414/intel-nuc.html)
 * The [Debian documentation](https://www.debian.org/doc/) particularly the section on [installing Debian](https://www.debian.org/releases/stable/installmanual)
 * The [Docker documentation](https://docs.docker.com/) particularly the sections on [installing docker on Debian](https://docs.docker.com/engine/install/debian/) and using [docker-compose](https://docs.docker.com/compose/)
 * The Grafana [home page](https://grafana.com/) and its [documentation](https://grafana.com/docs/)
 * The Graphite [home page](https://graphiteapp.org/) and its [documentation](https://graphite.readthedocs.io/en/latest/)