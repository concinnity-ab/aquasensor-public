#!/bin/bash
#This will install the latest version of Grafsy; last tested with 2.0.3
if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi
#set up service
cp -v ../config/grafsy.service /etc/systemd/system/
#Grafsy is written in go
apt-get -y install golang libacl1-dev
mkdir -p /opt/go/src /opt/go/bin /opt/go/pkg
export GOPATH=/opt/go
#this build needs libacl1-dev for a <sys/acl.h> include:
go get github.com/leoleovich/grafsy
cd /opt/go/src/github.com/leoleovich/grafsy && go get ./...
go install github.com/leoleovich/grafsy
mkdir -p /etc/grafsy && cp /home/pi/aquasensor/config/grafsy.toml /etc/grafsy/
#start it
systemctl enable grafsy

