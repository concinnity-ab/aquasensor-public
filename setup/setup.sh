#!/bin/bash
##For now, you must manually add one of these overlays to /boot/config.txt
#dtoverlay=w1-gpio, gpiopin=4 or
#dtoverlay=w1-gpio, gpiopin=5
##
##Similarly for a RTC overlay:
##Afterthought Software RasClock:
#dtoverlay=i2c-rtc,pcf2127
##The DS3231 also has very good accuracy:
#dtoverlay=i2c-rtc,ds3231

##Install packages
apt-get install python3 emacs gammu python3-gammu collectd
##Install Python libraries (if you are short on disk space, you can skip those for hardware you do not have) 
pip3 install board adafruit-blinka adafruit-circuitpython-tsl2561 adafruit-dht adafruit-circuitpython-am2320 adafruit-circuitpython-si7021 ads1015 adafruit-circuitpython-bmp280 adafruit-circuitpython-bme280 pagerduty_events_api pyephem crcmod pms5003 pymodbus

##copy the gammu config (only if you have a Huawei ???? SMS modem)
#cp -v ../config/.gammurc /home/pi
##Switch the 3G modem to Serial-port mode (not Memory mode, nor Ethernet mode) so Gammu can use it
#cp -v ../config/12d1\:1f01 /etc/usb_modeswitch.d/

#copy the collectd config
cp -v ../config/collectd.conf /etc/collectd/
cp -v ../config/filters.conf /etc/collectd/collectd.conf.d/
cp -v ../config/thresholds.conf /etc/collectd/collectd.conf.d/
systemctl restart collectd

#set up service
cp -v ../config/sensornode.service /etc/systemd/system/
systemctl enable sensornode
